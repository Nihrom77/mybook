<?php
include('../config/security.php'); session_start();
include('../global/mySql.php');

$idUser = $_SESSION['id_online_user'];
$query = "SELECT *   FROM `users` where `id`='$idUser' LIMIT 1";
$result = mysqli_query($dbc,$query); 
if(!$result){
	exit(mysqli_error($dbc));
}
if (mysqli_num_rows($result) > 0){
	$myrow = mysqli_fetch_array($result);
	$NAME=$myrow["Name"];
/* переменные для вывода разметки */
	$outputFutureBooks = '<hr>';
	$outputNowBooks = '<hr>';
	$outputLaterBooks = '<hr>';	
	$outputMyBooks = '<hr>';
/*три временные переменные для названий книг */
	$titleFutureBooks = '<ol>';
	$titleoutputNowBooks = '<ol>';
	$titleoutputLaterBooks = '<ol>';	
	$number=1;
	/*		Обращаемя к таблицам	
	books_read_future		
	books_read_now	
	books_read_later
	чтобы вывести 3 списка книг: прочитал, читает, когда-нибудь прочитает		*/
	$queryBooksFuture="SELECT `book_id` FROM `books_read_future` WHERE `user_id`='$idUser'";
	$queryBooksNow="SELECT `book_id` FROM `books_read_now` WHERE `user_id`='$idUser'";	
	$queryBooksLater="SELECT `book_id` FROM `books_read_later` WHERE `user_id`='$idUser'";	
	$queryBooksMy = "SELECT * FROM `books` WHERE `published`=0 AND `whoAdd`='$idUser'";
	
	/* список книг, которые пользователь читает  в будущем */
	$result = mysqli_query($dbc,$queryBooksFuture);	if(!$result){
		exit(mysqli_error($dbc));		die("error");	
	}
	if (mysqli_num_rows($result) > 0){
		$myrow = mysqli_fetch_array($result);
		do{
			$currBookId = $myrow["book_id"];	
			$queryB = "SELECT *   FROM `books` WHERE `book_id`='$currBookId' ";	
			$resultB = mysqli_query($dbc,$queryB);
			if(!$resultB){
				echo    "";		
				exit(mysqli_error($dbc));							
			}
			$rowB = mysqli_fetch_array($resultB);	
			$book_id=$rowB["book_id"];
			$title=$rowB["title"];	
			$titleFutureBooks = $titleFutureBooks.'<li>'.$title;
			$diskname=$rowB["diskname"];	
			$author=$rowB["author"];
			$category=$rowB["category"];	
			$rating=$rowB["rating"];	
			$book_cover=$rowB["img_cover"];
			$outputFutureBooks =$outputFutureBooks .'
		
			<div   class="books__item">
								<img   border="0"  src="'.$book_cover.'"       alt="'.$author.' - '.$title.'"   width="128"   height="198"   class="">
								<div   class="books__item__info">
									<span   class="books__item__info-name">
										<a   class="size1 hyphens"                  href="readMore.php?book='.$diskname.'"   title="'.$title.'">
											'.$title.'										
										</a>
																			
									</span>
									<span   class="books__item__info-authors">
										<a   href="readMore.php?book='.$diskname.'">
											'.$author.'										
										</a>
																			
									</span>
									<div   class="books__item__info_bottom">
										<div   class="button-container button-mod6">
											<div   class="button-container-bg">
												<div   class="button small">
													<div   class="tools_book">   <a   href="readBook.php?page=0&book='.$diskname.'">Читать</a></div>
																									
												</div>
																							
											</div>
											
										</div>
										<span   class="books__item__info-add">
											<a   href="#"    class="alt1 pseudo_link" onClick="addToMyBooks('.$book_id.');">
												<ins   class="with-icon with-icon_addbook">
													
												</ins>
												Добавить к моим книгам															
											</a>
																					
										</span>
																			
									</div>
																	
								</div>
															
							</div>


';
						    		
		}
		while ($myrow = mysqli_fetch_array($result));
		$titleFutureBooks = $titleFutureBooks.'</ol>';	
	}
	
	/* список книг, которые пользователь читает прямо сейчас */
	$result = mysqli_query($dbc,$queryBooksNow);	if(!$result){
		exit(mysqli_error($dbc));		die("error");	
	}
	if (mysqli_num_rows($result) > 0){
		$myrow = mysqli_fetch_array($result);	do{
			$number=1;		
$currBookId = $myrow["book_id"];	
	$queryB = "SELECT *   FROM `books` where `book_id`='$currBookId'";	
	$resultB = mysqli_query($dbc,$queryB);	
	if(!$resultB){
				echo    "";	
		exit(mysqli_error($dbc));							
			}

	$rowB = mysqli_fetch_array($resultB);	
	$book_id=$rowB["book_id"];
$title=$rowB["title"];	
	$titleNowBooks = $titleNowBooks.'
<li>'.$title;
		$diskname=$rowB["diskname"];		
$author=$rowB["author"];	
	$category=$rowB["category"];	
	$rating=$rowB["rating"];
	$book_cover=$rowB["img_cover"];
	$outputNowBooks =$outputNowBooks .'

<div   class="books__item">
								<img   border="0"  src="'.$book_cover.'"       alt="'.$author.' - '.$title.'"   width="128"   height="198"   class="">
								<div   class="books__item__info">
									<span   class="books__item__info-name">
										<a   class="size1 hyphens"                  href="readMore.php?book='.$diskname.'"   title="'.$title.'">
											'.$title.'										
										</a>
																			
									</span>
									<span   class="books__item__info-authors">
										<a   href="readMore.php?book='.$diskname.'">
											'.$author.'										
										</a>
																			
									</span>
									<div   class="books__item__info_bottom">
										<div   class="button-container button-mod6">
											<div   class="button-container-bg">
												<div   class="button small">
													<div   class="tools_book">   <a   href="readBook.php?page=0&book='.$diskname.'">Читать</a></div>
																									
												</div>
																							
											</div>
											
										</div>
										<span   class="books__item__info-add">
											<a   href="#"    class="alt1 pseudo_link" onClick="addToMyBooks('.$book_id.');">
												<ins   class="with-icon with-icon_addbook">
													
												</ins>
												Добавить к моим книгам															
											</a>
																					
										</span>
																			
									</div>
																	
								</div>
															
							</div>
					
';		$title=$rowB["title"];		$diskname=$rowB["diskname"];				    		
		}
		while ($myrow = mysqli_fetch_array($result));	$titleNowBooks = $titleNowBooks.'</ol>';	
	}
	
	/* список книг, которые пользователь уже прочитал */
	$result = mysqli_query($dbc,$queryBooksLater);	if(!$result){
		exit(mysqli_error($dbc));		die("error");	
	}
	if (mysqli_num_rows($result) > 0){
		$myrow = mysqli_fetch_array($result);	do{
			$number=1;		$currBookId = $myrow["book_id"];		$queryB = "SELECT *   FROM `books` where `book_id`='$currBookId'";		$resultB = mysqli_query($dbc,$queryB);		if(!$resultB){
				echo    "";			exit(mysqli_error($dbc));							
			}
			$rowB = mysqli_fetch_array($resultB);
			$book_id=$rowB["book_id"];
	$title=$rowB["title"];		
$titleLaterBooks = $titleLaterBooks.'<li>'.$title;	
	$diskname=$rowB["diskname"];	
	$author=$rowB["author"];	
	$category=$rowB["category"];		
$rating=$rowB["rating"];	
$book_cover=$rowB["img_cover"];
$outputLaterBooks =$outputLaterBooks .'

<div   class="books__item">
								<img   border="0"  src="'.$book_cover.'"       alt="'.$author.' - '.$title.'"   width="128"   height="198"   class="">
								<div   class="books__item__info">
									<span   class="books__item__info-name">
										<a   class="size1 hyphens"                  href="readMore.php?book='.$diskname.'"   title="'.$title.'">
											'.$title.'										
										</a>
																			
									</span>
									<span   class="books__item__info-authors">
										<a   href="readMore.php?book='.$diskname.'">
											'.$author.'										
										</a>
																			
									</span>
									<div   class="books__item__info_bottom">
										<div   class="button-container button-mod6">
											<div   class="button-container-bg">
												<div   class="button small">
													<div   class="tools_book">   <a   href="readBook.php?page=0&book='.$diskname.'">Читать</a></div>
																									
												</div>
																							
											</div>
											
										</div>
										<span   class="books__item__info-add">
											<a   href="#"    class="alt1 pseudo_link" onClick="addToMyBooks('.$book_id.');">
												<ins   class="with-icon with-icon_addbook">
													
												</ins>
												Добавить к моим книгам															
											</a>
																					
										</span>
																			
									</div>
																	
								</div>
															
							</div>
';	
	$title=$rowB["title"];		$diskname=$rowB["diskname"];				    		
		}
		while ($myrow = mysqli_fetch_array($result));
	$titleLaterBooks = $titleLaterBooks.'</ol>';
	}
	
		/* список книг, которые пользователь пишет прямо сейчас */
	$result = mysqli_query($dbc,$queryBooksMy);
	if(!$result){
		exit(mysqli_error($dbc));		die("error");	
	}
	if (mysqli_num_rows($result) > 0){
		$rowB = mysqli_fetch_array($result);
		do{
	$number=1;		

		
	$book_id=$rowB["book_id"];
	$title=$rowB["title"];	
	$diskname=$rowB["diskname"];		
	$author=$rowB["author"];	
	$category=$rowB["category"];	
	$rating=$rowB["rating"];
	$book_cover=$rowB["img_cover"];
	$outputMyBooks =$outputMyBooks .'

<div   class="books__item">
								<img   border="0"  src="'.$book_cover.'"       alt="'.$author.' - '.$title.'"   width="128"   height="198"   class="">
								<div   class="books__item__info">
									<span   class="books__item__info-name">
										<a   class="size1 hyphens"                  href="readMore.php?book='.$diskname.'"   title="'.$title.'">
											'.$title.'										
										</a>
																			
									</span>
									<span   class="books__item__info-authors">
										<a   href="readMore.php?book='.$diskname.'">
											'.$author.'										
										</a>
																			
									</span>
									<div   class="books__item__info_bottom">
										<div   class="button-container button-mod6">
											<div   class="button-container-bg">
												<div   class="button small">
													<div   class="tools_book">   <a   href="readBook.php?page=0&book='.$diskname.'">Читать</a></div>
																									
												</div>
																							
											</div>
											
										</div>
										<span   class="books__item__info-add">
											
 
											<a   href="#"    class="alt1 pseudo_link" onClick="editBook(\''.$diskname.'\');">
												<ins   class="with-icon with-icon_addbook">
													
												</ins>
												Прододить редактирование															
											</a>
																					
										</span>
																			
									</div>
																	
								</div>
															
							</div>
					
';		$title=$rowB["title"];		$diskname=$rowB["diskname"];				    		
		}
		while ($rowB = mysqli_fetch_array($result));	
	}
	}	?>