<?php
session_start();
class ParserFb2{
	const PAGE_SYMBOLS = 5000;
	var $bookFb2,    
	$content,    
	$bookName,    
	$authors,    
	$annotation,    
	$avatarBinary,
	$genres = array(),
	$pages = array(),    
	$tagsP = array(),    
	$tagsSection = array();   
	
	function    __construct($bookFb2) {		
		//require_once ('tx2fb.php');		
		$text = $_SERVER['DOCUMENT_ROOT'].$bookFb2;         	
		$xsltfile = $_SERVER['DOCUMENT_ROOT'].'/handler/FB2_22_xhtml.xsl';
		$xml = new DOMDocument(); 	$load = $xml->load($text); 	if (!$load) {
			//echo "Ошибка загрузки!"; 					
		}
		$xsl = new DOMDocument(); 	$load = $xsl->load($xsltfile); 	if (!$load) {
			//echo "Ошибка загрузки!"; 					
		}
		$proc = new XSLTProcessor;
		$proc->importStylesheet($xsl);
		$this->content = $proc->transformToXml($xml);
		$description = $xml->getElementsByTagName('description')->item(0);
		if (!$description) {
			//echo "No description!"; 					
		}
		$title_info = $description->getElementsByTagName('title-info')->item(0);
		
		$genres_list = $title_info->getElementsByTagName('genre');
		if (count($genres_list) != 0){ 
			$elemG = '';
			foreach ($genres_list as $elemG) {
				if($elemG){
					$gs[] = $elemG -> nodeValue;
					// echo($elemG->nodeValue);
				} else{
					break;
				}
			}
			$this->genres = $gs;
		}
		// echo($this->genres[0]);
		
		$authors_list = $title_info->getElementsByTagName('author');		
		if (count($authors_list)!=0){
				$element = '';
			foreach ($authors_list    as    $element) {
				$firstNameDoc = $element->getElementsByTagName('first-name')->item(0);
				$lastNameDoc = $element->getElementsByTagName('last-name')->item(0);
				$nickNameDoc = $element->getElementsByTagName('nickname')->item(0);
				$middleNameDoc = $element->getElementsByTagName('middle-name')->item(0);
				$emailDoc = $element->getElementsByTagName('email')->item(0);
				$authors[] =  new Author(
					$firstNameDoc? $firstNameDoc->nodeValue : null,
					$lastNameDoc? $lastNameDoc->nodeValue : null,				     
					$middleNameDoc? $middleNameDoc->nodeValue : null,				    
					$nickNameDoc? $nickNameDoc->nodeValue : null,				     
					$emailDoc? $emailDoc->nodeValue : null
				);					
			}
			$this->authors = $authors;
		}
		
		$this->bookName = $title_info->getElementsByTagName('book-title')->item(0)->nodeValue;
		$this->annotation = $title_info->getElementsByTagName('annotation')->item(0)->nodeValue;
		$this->avatarBinary = $xml->getElementsByTagName('binary')->item(0)->nodeValue;
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		$file = $_SERVER['DOCUMENT_ROOT'].$bookFb2;
		$this->bookFb2 = simplexml_load_file    ($file);
		$this->content = '';
		$book = $this->bookFb2->body;
		foreach ($book->children()    as    $key=>$body){
			$this->content .= $this -> getString($key,$book);							
		}
		$this->pages = $this->getPages($this->content);
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		$this->tagsP = $this -> getAllTagsP($xml);
		$this->tagsSection = $this -> getAllTagsSection($xml);			
	}
	
	private function    getAllTagsP($cont){
		return    $cont -> getElementsByTagName('p');			
	}
	
	private function    getAllTagsSection($cont){
		return    $cont -> getElementsByTagName('section');			
	}
	
	public function    getTagsP($start, $numberTags, $cont = ''){
		for($i = $start; $i<$start+$numberTags; $i++){
			$cont .= '<br/>'.$this->tagsP->item($i)->nodeValue;
					
		}
		return    $cont;			
	}
	
	public function    getTagsSection($start, $numberTags, $cont = ''){
		for($i = $start; $i<$start+$numberTags; $i++){
			$cont .= $this->tagsSection->item($i)->C14N();					
		}
		$cont = preg_replace('</?title>', 'br/', $cont);	
		return    $cont;			
	}
	
	public function getGenres(){
		return $this->genres;
	}
	
	private function    getPages($string){
		$pages = array();
		$length = strlen($string);
		$mark = false;
		for($i=0, $currentPageNumber = 0,$step=1; $i < $length; $currentPageNumber++, $step++){
			$pages[$currentPageNumber] = '';
			for(;
			$i < $length; $i++){
				$pages[$currentPageNumber] .= $string[$i];
				if($i == $step*ParserFb2::PAGE_SYMBOLS) {
					$mark = true;
									
				}
				if($mark    &&    $string[$i] === ' '){
					$mark = false;
					$i++;
					break;							
				}					
			}			
		}
		return    $pages;	
	}

	private function    getString($key, $body){
		switch ($key){
			case    'image':
				//$this->print_image ($body);
				return    '';
			case    'title':
				//if ($i == 0){$i=1;}
				//return ''.$this->__clear_string($body->asXML()).'';
				return    '';
				break;
			case    'section':
				return    '';
			case    'epigraph':
				return    ''.$this->__clear_string($body->asXML());
			default:
				return    ''.$this->__clear_string($body->asXML(), false);											
		}									
	}
	
	private function    __clear_string ($string, $p = true){
		$string = preg_replace('/\|\<\/title\>/im', '', $string); //|\|\<\/p\>
		$string = preg_replace('/\s+/m', " ", $string);
		$string = preg_replace('/^\s+|\s+$/', '', $string);
		if ($p){
			$string = preg_replace('/\|\<\/p\>/im', '', $string);											
		}
		return    $string;									
	}
	
	public function    getContent(){
		return    $this->content;									
	}
	
	public function    getPage($pageNumber){
		return    $this->pages[$pageNumber];									
	}
	
	public function    getBookName(){
		return    $this->bookName;									
	}
	
	public function    getAuthors(){
		return    $this->authors;									
	}
	
	public function    getAnnotation(){
		return    $this->annotation;									
	}
	
	public function    getAvatarBinary(){
		return    $this->avatarBinary;									
	}							

}
			
class Author{
	var $firstName,	$lastName,	$middleName,	$nickName,	$email;
	
	function    __construct($firstName, $lastName, $middleName, $nickName, $email){
		$this->firstName = $firstName;
		$this->lastName = $lastName;
		$this->middleName = $middleName;
		$this->nickName = $nickName;
		$this->email = $email;									
	}
	
	public function    getFirstName(){
		return    $this->firstName;									
	}
			
	public function    getLastName(){
		return    $this->lastName;									
	}
			
	public function    getMiddleName(){
		return    $this->middleName;									
	}
	
	public function    getNickName(){
		return    $this->nickName;									
	}
			
	public function    getEmail(){
		return    $this->email;									
	}							
}

?>