<?php
class ParserFb2{
    
    const PAGE_SYMBOLS = 2500;
    var $bookFb2,
    $content,
    $bookName,
    $authors,
    $annotation,
    $avatarBinary;

    function __construct($bookFb2) {
	//require_once ('tx2fb.php');
	$text = $_SERVER['DOCUMENT_ROOT'].$bookFb2;        
 	$xsltfile = $_SERVER['DOCUMENT_ROOT'].'/handler/FB2_22_xhtml.xsl';

	$xml = new DOMDocument(); 
	$load = $xml->load($text); 
	if (!$load) { 
	  //echo "Ошибка загрузки!"; 
	}
        
        $xsl = new DOMDocument(); 
	$load = $xsl->load($xsltfile); 
	if (!$load) { 
	  //echo "Ошибка загрузки!"; 
	}
        
	$proc = new XSLTProcessor;
        $proc->importStylesheet($xsl);
	$this->content = $proc->transformToXml($xml);
	
	$description = $xml->getElementsByTagName('description')->item(0);
	if (!$description) {
	    //echo "No description!"; 
	}

	$title_info = $description->getElementsByTagName('title-info')->item(0);

	$authors_list = $title_info->getElementsByTagName('author');	
	if (count($authors_list)==0){
	  //echo "Ошибка загрузки!"; 
	}
	$element = '';
	foreach ($authors_list as $element) {

	    $firstNameDoc = $element->getElementsByTagName('first-name')->item(0);
	    $lastNameDoc = $element->getElementsByTagName('last-name')->item(0);
	    $nickNameDoc = $element->getElementsByTagName('nickname')->item(0);
	    $middleNameDoc = $element->getElementsByTagName('middle-name')->item(0);
	    $emailDoc = $element->getElementsByTagName('email')->item(0);

	    $authors[] =  new Author($firstNameDoc? $firstNameDoc->nodeValue : null,
				     $lastNameDoc? $lastNameDoc->nodeValue : null,
				     $middleNameDoc? $middleNameDoc->nodeValue : null,
				     $nickNameDoc? $nickNameDoc->nodeValue : null,
				     $emailDoc? $emailDoc->nodeValue : null);
	}
	$this->authors = $authors;

	$this->bookName = $title_info->getElementsByTagName('book-title')->item(0)->nodeValue;

	$this->annotation = $title_info->getElementsByTagName('annotation')->item(0)->nodeValue;
	
	$this->avatarBinary = $xml->getElementsByTagName('binary')->item(0)->nodeValue;
    }

    public function getContent(){
        return $this->content;
    }
	public function getPage($pageNum){
return $this->content;
}
    public function getBookName(){
	return $this->bookName;
    }

    public function getAuthors(){
	return $this->authors;
    }

    public function getAnnotation(){
	return $this->annotation;
    }
   
    public function getAvatarBinary(){
	return $this->avatarBinary;
    }

}

class Author{
    
    var $firstName,
	$lastName,
	$middleName,
	$nickName,
	$email;

    function __construct($firstName, $lastName, $middleName, $nickName, $email){
	$this->firstName = $firstName;
	$this->lastName = $lastName;
	$this->middleName = $middleName;
	$this->nickName = $nickName;
	$this->email = $email;
    }

    public function getFirstName(){
	return $this->firstName;
    }

    public function getLastName(){
	return $this->lastName;
    }
    
    public function getMiddleName(){
	return $this->middleName;
    }

    public function getNickName(){
	return $this->nickName;
    }

    public function getEmail(){
	return $this->email;
    }

}
?>
