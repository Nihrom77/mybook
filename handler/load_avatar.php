<?php
$extensions = array('jpeg', 'jpg', 'png', 'gif');
$max_size = 500000;
$path = $_SERVER['DOCUMENT_ROOT'].'/avatar/';
$response = '';
if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
  if ($_FILES['image']['size'] > $max_size)
  {
    $response = 'File is too large';
  }
  else
  {
    $ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
    if (in_array($ext, $extensions))
    {
      $path = $path . uniqid() . '.' . $ext;
 
      if (move_uploaded_file($_FILES['image']['tmp_name'], $path))
      {
        $response = "<img style='height: 200px' src='$path' />";
      }
    }
    else
    {
      $response = 'File must be an image!';
    }
  }
}
 
echo $response;