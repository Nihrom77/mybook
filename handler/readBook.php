<?php
session_start();
include('../config/security.php');
include  ('../handler/parserFb2.php');
?>
<!DOCTYPE  HTML  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta  http-equiv="Content-Type"  content="text/html; charset=utf-8" />		<title>
			myBook | Описание к книге
		</title>
		<link  rel="stylesheet"  href="../CSS/style.css"  type="text/css" />	
			<link  href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,700,400&subset=latin,cyrillic'  rel='stylesheet'  type='text/css' />
			<link rel="stylesheet" href="../CSS/colorpicker.css" type="text/css" />
			<link rel="stylesheet" href="../CSS/layout_colorpicker.css" type="text/css" />
		
			<script  src="../js/jQuery1-3.js"></script>
		<script type="text/javascript" src="../js/colorpicker.js"></script>
    <script type="text/javascript" src="../js/eye.js"></script>
    <script type="text/javascript" src="../js/utils.js"></script>
    <script type="text/javascript" src="../js/layout.js?ver=1.0.2"></script>
		</script>
		<style>
			.panel_UL {
				padding:0px; margin:0px; list-style:none;
				
			}
			.panel_UL li {
				float:left; padding:10px;
				
			}
			.font_Arial {
				font-family:Arial;
			}
			.font_Tahoma {
				font-family:Tahoma;
			}
			.font_Times {
				font-family: Times;
			}
			
		</style>
		<!-- 
				нужно будет в отдельный файл сделать например read_panel.js
					-->
		
		
	</head>
	<body style="background:#fff;">

		<?php
		require_once('../global/header.php');
		?>
		<?php
							require_once('../global/menu.php');
			?>
			<div style="padding-left: 65px; width:auto; padding-right: 20px;">			
				<table  border="0"  width="100%"   class="table">
										<tr>
											<td  valign="top"  align="left">
												<h1>
													<a  href="bibleotek.php">
														Библиотека
													</a>
													&raquo; Чтение книги 
												</h1>
												<br />												  
											</td>
											
										</tr>
									</table>
				<div  style=" padding:10px; width:auto;font-size:11pt; display: block;"  align="left">
										<?php
										$cur_page = isset($_GET['page']) ? $_GET['page'] : 0; if($cur_page<0){
											$cur_page = 0;
										}
										$cur_book = $_GET['book'];
										?>

										<a  href="readBook.php?page=<?php	echo  ($cur_page-1).'&book='.$cur_book; ?>">Предыдущая страница</a> 
									
										| <h2  style="display:inline">	Вы читаете <?php	echo  $cur_page+1;	?>	страницу</h2>
										| <a  href="readBook.php?page=<?php 	echo  ($cur_page+1).'&book='.$cur_book; ?>">Следующая страница</a>
									
										
									</div>					
				<div  style="width:auto; display:block; height:50px;">
										<ul  class="panel_UL">
											<li>
												Шрифт: <select  id="shrift">
													<option  value="Arial">
														Arial
													</option>
													<option  value="Tahoma">
														Tahoma
													</option>
													<option  value="Times">
														Times
													</option>
													
												</select>
											</li>
																			<li>
												Размер шрифта: <select  id="size_text">
													<option  value="12px">
														12px
													</option>
													<option  value="13px" selected="selected">
														13px
													</option>
													<option  value="14px">
														14px
													</option>
													<option  value="15px">
														15px
													</option>
													<option  value="16px">
														16px
													</option>
													<option  value="17px">
														17px
													</option>
												</select>
											</li>
											<li>
												<table>
													<tr>
														<td valign="top">Цвет фона:</td>
														<td valign="top"><div id="colorSelector" style="display:inline-block;"><div style="background-color: #ffffff"></div></div></td>
													</tr>
												</table>
											</li>
											<li>
												Цвет текста: <input type="text" maxlength="6" size="6" id="colorpickerField1" value="000000" />
											</li>
											<li>
												<input  type="button"  value="Применить"   onclick="changeStyleText();" />								
											</li>

										</ul>
										
									</div>
								
									<div  class="readText"  id="readText" align="left">
										<?php
										$parser = new ParserFb2('/book/'.$cur_book);
										$next_page = $_GET['page'];
										if($next_page<0){
											echo  $parser->getTagsSection(1);
										}
										else {
											echo  $parser->getTagsSection(1+$next_page);
										}
										?>
										
									</div>
		</div>
		<!-- BODY Here -->

<script>
		<?php
	
		 	 $color=$_SESSION['set_color'];
			$font_family = $_SESSION['set_font_family'];
			$font_size =  $_SESSION['set_font_size'];
			 $background = $_SESSION['set_background'];
		 	?>
		 	
		 	$('.readText').css({"font-size":<?php echo "'$font_size'";?>,'color':<?php  echo "'$color'"; ?>});
		 	$('body').css({"background-color": <?php echo "'$background'"; ?>});
		 	$('.readText').addClass('font_'+<?php echo "'$font_family'";?>);

			function  changeStyleText() {
				$(".readText").removeClass("font_Arial font_Tahoma font_Times");
				var background_text = $('#colorSelector div').css('backgroundColor');
				var color_text = '#'+$('#colorpickerField1').val();
				$('.readText').css({"font-size":$('#size_text').val(),'color':color_text});
				$('.readText').addClass('font_'+$('#shrift').val());
				$('body').css({"background-color": background_text});
				postSettings();
			}
			function postSettings() {
  $.post(
  "/handler/settings_read.php",
  {
    color: '#'+$('#colorpickerField1').val(),
    fontfamily: $('#shrift').val(),
    fontsize: $('#size_text').val(),
    background: $('#colorSelector div').css('backgroundColor'),
    
    
  },
  onAjaxSuccess
  );
}

function onAjaxSuccess(data){
  if (data=="Настройки сохранены"){
        
  } else {	
alert(data);
  }
}

			
		</script>
			<script>
			$('#colorpickerField1').ColorPicker({
				color:'#00000',
				onSubmit: function(hsb, hex, rgb, el) {
				$(el).val(hex);
				$(el).ColorPickerHide();
			},
			onBeforeShow: function () {
				$(this).ColorPickerSetColor(this.value);
			},
			onChange: function (hsb, hex, rgb) {
			$('#colorpickerField1').val(hex);
		$('#colorpickerField1').css({'color':'#' + hex,'backgroundColor':$('#colorSelector div').css('backgroundColor')});
	}
		})
		.bind('keyup', function(){
			$(this).ColorPickerSetColor(this.value);
		});

	$('#colorSelector').ColorPicker({
	color: '#ffffff',
	onShow: function (colpkr) {
		$(colpkr).fadeIn(500);
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut(500);
		return false;
	},
	onChange: function (hsb, hex, rgb) {
		$('#colorSelector div').css('backgroundColor', '#' + hex);
	}
});
			</script>
	</body>
</html>