<?php
header('Content-Type: text/html; charset=utf-8');
$allowedExts = array("fb2");
$diskname = md5(uniqid(""));
$file_ext = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
$answerRes = "";
//$extension = end(explode(".", $_FILES["file"]["name"])); 
// if (($_FILES["file"]["type"] != "application/octet-stream") ){
// die ("Неверный формат файла");	
// }
if ($_FILES["file"]["size"] > 52428800) {
	die("Файл слишком велик, максимальный размер =50Мб");	    
}
if (!in_array($file_ext, $allowedExts)) {
	die("На сайте позволен только fb2 формат");	    
}
if ($_FILES["file"]["error"] > 0)     {
	die( "Return Code: " . $_FILES["file"]["error"] . "<br>");     
}
include('../global/mySql.php');
include('../handler/parserFb2.php');
move_uploaded_file($_FILES["file"]["tmp_name"],"../book/" . $diskname);
$p = new ParserFb2('/book/'.$diskname);
$title=$p->getBookName();
$autorsArr = $p->getAuthors();
$author=($autorsArr[0]->getFirstName())." ".($autorsArr[0]->getLastName());
//проверим не добавляли ли такую книгу
mysqli_query($dbc, "SELECT * FROM `books` WHERE `title`='$title' AND `author`='$author'")    or   die   ('Error: ' . mysqli_error($dbc));
if (mysqli_affected_rows($dbc)>0)         {
	unlink('../book/'.$diskname);
	die( " This book already exists. ");                  
}
session_start();
$myID = $_SESSION['id_online_user'];
$category= $p->getGenres()[0];
$img=$p->getAvatarBinary();
$img_cover_url='/book/'.$diskname.'.jpg';
$decoded=base64_decode($img);
file_put_contents('..'.$img_cover_url,$decoded);
mysqli_query($dbc,'SET NAMES utf8');
$query = "INSERT INTO `books` (`format`, `diskname`, `title` ,`author` ,`category`, `whoAdd`, `img_cover`, `published`) VALUES ('$file_ext', '$diskname', '$title' , '$author', '$category' ,'$myID', '$img_cover_url','1')";
$sql = mysqli_query($dbc,$query)   or   die   ('Error: ' . mysqli_error($dbc));
echo   '
	<div  class="material">	
		<div   class="title_book"   align="center">
			'.$title.'
		</div>				
		<div   class="img_book"   align="center">							
			<a   href="../page/readMore.php?book='.$diskname.'"   title="Описание">		
				<img   src="'.$img_cover_url.'"   width="215"   border="0" />		
			</a>											
		</div>								
		<div   class="tools_book">		
			<a   href="../page/readMore.php?book='.$diskname.'"   title="Описание">		
				Описание					
			</a>
			<a   href="../page/readBook.php?page=0&book='.$diskname.'">	
				Читать											
			</a>				
		</div>	
	</div>		
	';
?>