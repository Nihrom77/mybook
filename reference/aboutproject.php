<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>About project | О проекте</title>
		<link rel="stylesheet" href="../CSS/style.css" type="text/css" />
		<link href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,700,400&subset=latin,cyrillic' rel='stylesheet' type='text/css' />
		<script src="../js/jquery.js"></script>
		<script src="../js/menu.js"></script>
<script src="../js/jQuery1-3.js"></script>
		<script src="../js/enter.js"></script>
	</head>
	<body>
		<div class="background"></div>

		<?php require_once('../global/header.php');?>

		<!-- BODY Here -->
			<div align="center" style="width:auto; min-height:1000px;">
			
			<div>
				<table width="980">
					<tr>
						<td valign="bottom">
					
						<div id="main-content">
							<div class="paper">
						<center><i><font color="#000000" size="6"><b>В моём шкафу теснится к тому том, <br>
И каждый том на полке словно дом… <br>
Обложку-дверь откроешь второпях –  <br>
И ты вошёл, и ты уже в гостях. <br>
Как переулок – каждый книжный ряд. <br>
И весь мой шкаф – чудесный Книгоград… <br>
(Д. Кугультинов)   </b></font>  </i></center><br/>
			
<br><br><br><br>  <font color="#000000" size="5"><b> Прекрасные строки калмыцкого советского поэта Давида Кугультинова как нельзя лучше отражают суть нашего проекта. Ведь каждая книга - это целый мир. И мы, разработчики данного проекта, стараемся сделать все эти прекрасные и захватывающие миры более доступными для каждого. </b></font>				</div>
						</div></td>
					</tr>
				</table>
			</div>
		</div>

		<!-- END BODY -->
	
		<?php require_once('../global/footer.php');?>
		

	</body>
</html>

