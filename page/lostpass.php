
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<head>
<title>Восстановление пароля</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="images/siteico0.ico" type="image/x-icon" />
<script src="../js/jQuery1-3.js"></script>
		<script src="../js/enter.js"></script>
<style type="text/css" media="all">
* { margin: 0; padding: 0; }
body {background: #fAfAfA; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px;}
img {border:none;}
#conteiner {width: 400px; margin: 7em auto;}
#conteiner .loform {
    padding: 16px 16px 16px 16px;
    font-weight: normal;
    -moz-border-radius: 11px;
    -khtml-border-radius: 11px;
    -webkit-border-radius: 11px;
    border-radius: 5px;
    background: #fff;
    border: 1px solid #e5e5e5;
    -moz-box-shadow: rgba(200,200,200,1) 0 4px 18px;
    -webkit-box-shadow: rgba(200,200,200,1) 0 4px 18px;
    -khtml-box-shadow: rgba(200,200,200,1) 0 4px 18px;
    box-shadow: rgba(200,200,200,1) 0 4px 18px;
}
#conteiner .mess {
    margin-bottom: 10px;
    padding: 10px;
    font-weight: normal;
    -moz-border-radius: 5px;
    -khtml-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    border: 1px solid;
    -moz-box-shadow: rgba(200,200,200,1) 0 4px 18px;
    -webkit-box-shadow: rgba(200,200,200,1) 0 4px 18px;
    -khtml-box-shadow: rgba(200,200,200,1) 0 4px 18px;
    box-shadow: rgba(200,200,200,1) 0 4px 18px;
    color:#000;
    font-family:Verdana, Arial, Helvetica, sans-serif;
    font-size:11px;
}


#conteiner .loform p {color:#808080; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:14px; margin:5px;}
#conteiner .loform input {background:#fafafa; border:1px solid #cccccc; color:#666666; padding:4px; width:98%; font-size:25px; margin-bottom:20px;}
#conteiner .loform input.submit {margin-top:-16px; width:74px; height:64px; border: 1 px; text-align:right; vertical-align:top;}
#conteiner .loform a {color: #bcbcbc; text-decoration: none;}
#conteiner .loform a:hover {color:#d7722f;}

#conteiner .logo {text-align:center; padding-bottom:20px;}
</style>
</head>
<body>
<div id="conteiner">
    <div class="logo">
      <p><strong>Восстановление пароля</strong></p>
    </div>

<div class="loform">
            <form name="form1" method="POST" >
            <p>E-mail <input type="text" id="email" size="40" /></p>
            <p>

                <input type="button" onClick="restorePassword();" value="востановить" size="40" name="go">

            </p>
        </form>
        
    </div>
<div id="error" style="text-align: center;font-size: 16px; color:red;"></div>
</div>
</body>
</html>
    

    
