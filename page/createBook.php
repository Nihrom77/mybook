<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
		<title>CreateBook | Написать книгу</title>
		<link rel="stylesheet" href="../CSS/style.css" type="text/css" >
		<link href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,700,400&subset=latin,cyrillic' rel='stylesheet' type='text/css' >

<script src="../js/jQuery1-3.js"></script>
		<script src="../js/enter.js"></script>
	</head>
	<body>
		<div class="background"></div>

		<?php require_once('../global/header.php');?>

		<!-- BODY Here -->
			<div align="center" style="width:auto; min-height:1000px;">
			
			<div>
				<table width="980">
					<tr>
						<td valign="bottom">
					<?php
						require_once('../global/menu.php');
						?>
						<div id="main-content">
							<div class="paper">
 							<!-- Контент -->
	 								<table      border="0"      width="100%"       class="table">
									<!-- Первая строка заголовок -->
									<tr>
										<td      valign="top"      align="left"      style="padding: 10px;">
											<div    align="left"    style="width:100%;">
												<h1>
													Написать новую книгу																																																		
												</h1>
												<a href="#">Написанные мной книги</a>
												
												
																							
											</div>
											<br/>																																																																													
										</td>
																			
									</tr>
									<tr>
										<td>
											<form  name="settings_form"   method="POST"  enctype="multipart/form-data"  action="/handler/addUserBook.php">
											<div>
											Название новой книги: 
											</div>
											<div>
												<input  type="text"  name="book_name"   value="" >
											</div>	
											<div>
											Жанр книги:
											</div>
												<input  type="text"  name="book_category"   value="" >
											<div>
												<label  for="file">Добавить обложку:	</label>
																 <input type="hidden" name="MAX_FILE_SIZE" value="52428800" />
															<input    type="file"    name="file_cover"    id="file">
											</div>
											<div>
												<input    type="submit"    name="submit"    value="Создать книгу">
											</div>
											
											</form>
										</td>
																			
									</tr>
									<tr>
										<td>
											
																					
										</td>
																			
									</tr>
																	
								</table>
							</div>
						</div></td>
					</tr>
				</table>
			</div>
		</div>

		<!-- END BODY -->
	
		<?php require_once('../global/footer.php');?>
		

	</body>
</html>

