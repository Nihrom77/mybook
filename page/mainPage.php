<?php
include('../config/security.php');
include('../global/mySql.php');
?>

<!DOCTYPE    HTML>
<head>
	<meta      http-equiv="Content-Type"      content="text/html; charset=utf-8" >
	<title>
		myBook | Главная						
	</title>
	<link      rel="stylesheet"      href="../CSS/style.css"      type="text/css" >
	<link   rel="stylesheet"   href="../CSS/book_cover_style.css"   media="all"   type="text/css" /> 
	<link      href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,700,400&subset=latin,cyrillic'      rel='stylesheet'      type='text/css' >
<script src="http://code.jquery.com/jquery-latest.min.js"
        type="text/javascript"></script>
	<script      src="../js/enter.js">
		
	</script>
	
</head>
<body>
	<div   class="background">
		
	</div>
	<?php
	require_once('../global/header.php');
	?>
	<!-- BODY Here -->
	<div      align="center"      style="width:auto; height:auto; min-height:1550px;">
		<div>
			<table      width="980">
				<tr>
					<td      valign="bottom">
						<?php
						require_once('../global/menu.php');
						?>
						<div      id="main-content">
							<div      class="paper"    style="height: auto; min-height: 1300px;">
								<table      border="0"      width="100%"       class="table">
									<!-- первая строка Новинки и популярные книги -->
									<tr>
										<td>
											<hr>
											<div>
												<center><h2>	Новинки и популярные книги</h2></center>
											</div>
											<!-- 5 самых популярных по рейтингу книг -->
											<?php require "../handler/getMostPopularBook.php"; ?>
											<div class="categories">
												<center><a href="/page/bibleotek.php">Все новые и популярные книги</a></center>
											</div>
										</td>
									</tr>
									<!-- Вторая строка Вся библиотека -->
									<tr>
										<td>
											<hr>
											<div  class="categories">
												<center><h2>Поиск по жанрам</h2></center>
											</div>
											<ul class="categories">
											<li class="link"><a href="/page/catalog.php?category=Детектив">Детективы</a></li>
											<li class="link"><a href="/page/catalog.php?category=Боевик">Боевики</a></li>
											<li class="link"><a href="/page/catalog.php?category=Фэнтези">Фэнтези</a></li>
											<li class="link"><a href="/page/catalog.php?category=Юмор">Юмор</a></li>
											<li class="link"><a href="/page/catalog.php?category=рассказ">Повести расказы</a></li>
											<li class="link"><a href="/page/catalog.php?category=Образование">Наука,Образование</a></li>
											<li class="link"><a href="/page/catalog.php?category=Классика">Классика</a></li>
											</ul>
										</td>
									</tr>
									<!-- Третья строка Лучшая рецензия и ссылка на соц сеть -->
									<tr>
										<td>
											<hr>
											<div>
											<center><h2>	Лучшая рецензия </h2></center>
											</div>
											<div>
												<select   id="period" >
												
													<option    value="allTime" >
														За все время																												
													</option>
												</select>
											</div>
											<div>
												<!-- Текст и книги рецензии -->
												<?php require "../handler/getBestComment.php"; ?>
											</div>
											<br><hr>
											<div>
												<!-- Здесь нужно подключить социальный плагин от контакта -->
												<script type="text/javascript" src="//vk.com/js/api/openapi.js?112"></script>

												<!-- VK Widget -->
												<div id="vk_groups"></div>
												<script type="text/javascript">
												VK.Widgets.Group("vk_groups", {mode: 0, width: "860", height: "300", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 71757028);
												</script>
											</div>
											<hr>
										</td>
									</tr>
								</table>
															
							</div>
							<!-- end paper -->
							<!-- здесь может быть ваша реклама -->
						
													
						</div>
						<!-- end main-content -->
											
					</td>
									
				</tr>
							
			</table>
					
		</div>
			
	</div>
	<!-- END BODY -->
		<?php
							require_once('../global/footer.php');
							?>
</body>
</html>