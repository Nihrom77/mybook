<!DOCTYPE  html>
<?php  include('../config/security.php');  
include('../global/mySql.php');  
session_start(); 
$id_user_now=$_SESSION['id_online_user']; // ид свой									
$query = "SELECT *   FROM `users` WHERE `id`=$id_user_now";
$result = mysqli_query($dbc,$query);
if(!$result){	echo   "<p class='text'>Такого пользователя не существует</p>";	
echo   exit(mysqli_error($dbc));
}
if (mysqli_num_rows($result) == 1){		$myrow = mysqli_fetch_array($result);				
$NAME=$myrow["Name"];		
$EMAIL=$myrow["Email"];		
$AVATAR_URL=$myrow["url_avatar"];		
$DATE = $myrow["date_of_birth"];		
$LOGIN = $myrow["login"];		
}
?>	<head>
	<meta    http-equiv="Content-Type"    content="text/html; charset=utf-8" />	
	<title>
		Настройки профиля - myBook
	</title>
	<link  rel="stylesheet"  href="../CSS/style.css"  type="text/css" />		<link  rel="stylesheet"  href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,700,400&subset=latin,cyrillic'   type='text/css' />		<script  src="../js/jQuery1-3.js">
	</script>
	<script  src="../js/enter.js">
	</script>
	<script  src="../js/client-interface.js">
	</script>
			<script>
		$(function () {
			$( "#dateGrid" ).datepicker("option", "dateFormat",'yy-mm-dd');
			
		});
	</script>
	<style>
		.table_profile {
			border: 1px solid #161614; display:block; border-radius:4px;
			
		}
		.top_profile_title {
			border-radius: 1px 1px 0 0;
			background: #161614; /* Old browsers */
			background: -moz-linear-gradient(top,  #161614 0%, #000000 100%); /* FF3.6+ */
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#161614), color-stop(100%,#000000)); /* Chrome,Safari4+ */
			background: -webkit-linear-gradient(top,  #161614 0%,#000000 100%); /* Chrome10+,Safari5.1+ */
			background: -o-linear-gradient(top,  #161614 0%,#000000 100%); /* Opera 11.10+ */
			background: -ms-linear-gradient(top,  #161614 0%,#000000 100%); /* IE10+ */
			background: linear-gradient(to bottom,  #161614 0%,#000000 100%); /* W3C */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#161614', endColorstr='#000000',GradientType=0 ); /* IE6-9 */
			display:block;
			height:auto; width:100%; padding:5px; font-size:18px; font-weight: bold; color: #fff;
			
		}
					/*
					 * MENU PROFILE CSS
					 */
		.menu_profile {
			padding:0px; margin:0px; list-style:none;
			
		}
		.menu_profile li a {
			display:block;
			padding:10px;
			text-decoration:none;
			text-align:left;
			color: #3e3c38;
			background:#e9e0d2;
			border-bottom:1px solid #c88255;
			
		}
		.menu_profile li a:hover {
			color:#fff; background:#000; border-left: 4px solid #F72E2E;
						
		}
		.raiting_user {
			background: #6bba70;
			display:block; width:auto; margin:5px; border:1px solid green; padding:2px; color:#fff; font-weight:bold; border-radius:10px;
			cursor:help;
			
		}
		.menu_under_avatar {
			padding:0px; margin:0px; list-style:none;
			
		}
		.menu_under_avatar li a {
			color:#2F302C; font-size:11px; text-decoration:none;
			text-align:left; padding:5px; border-bottom: 1px solid #8b8b8b;
			background:#e9e0d2; display:block; width:100%;
			
		}
		.menu_under_avatar li a:hover {
			background:#000; color:#fff;
			
		}
				
	</style>
	
</head>
<body>
<div  class="background"></div>
	<?php require_once('../global/header.php');?>		<!-- BODY Here -->
	<div  align="center"  style="width:auto; min-height:1000px;">
		<div>
			<table  width="980">
				<tr>
					<td  valign="bottom">
						<div  id="main-content">
							<div  class="paper">
								<div  class="table_profile">
									<table  width="100%"  cellpadding="0"  cellspacing="0">
										<tr>
											<td  colspan="2"  align="right">
												<div   class="top_profile_title">
													Настройки : <?php  echo $NAME;?>
												</div>
											</td>
											
										</tr>
										<tr>
											<td  valign="top"  align="left"  width="20%"   height="600"  style="background:#e9e0d2;">
												<?php require_once ('../global/menu_profile.php'); ?>																					
											</td>
											<td  valign="top"  align="left"  width="80%">
												<form  name="settings_form"   method="POST"  enctype="multipart/form-data"  action="/handler/editProfile.php">
													<table  border="0"  cellpadding="10"  cellspacing="0"  width="100%">
														<tr>
															<td  width="30%"  valign="top"  align="right">
																Ваше имя: 
															</td>
															<td  width="70%"   align="left">
																<input  type="text"  name="name_edit"   value="<?php echo $NAME;?>" >
															</td>
															
														</tr>
														<tr>
															<td  width="30%"  valign="top"  align="right">
																Ваш логин: 
															</td>
															<td  width="70%"  align="left">
																<input  type="text"   name="login_edit"  value="<?php echo $LOGIN;?>"  >
															</td>
															
														</tr>
														<tr>
															<td  width="30%"  valign="top"  align="right">
																Ваш новый пароль: 
															</td>
															<td  width="70%"   align="left">
																<input  type="password"  name="password_edit"  value="" >
															</td>
															
														</tr>
														<tr>
															<td  width="30%"  valign="top"  align="right">
																Ваша дата рождения: 
															</td>
															<td  width="70%"   align="left">
																<input  type="text"  name="date_edit"  value="<?php echo $DATE;?>"  id="dateGrid" >
															</td>
															
														</tr>
														<tr>
															<td  width="30%"  valign="top"  align="right">
																Ваш E-mail: 
															</td>
															<td  width="70%"   align="left">
																<input  type="text"  name="email_edit"  value="<?php echo $EMAIL;?>" >
															</td>
															
														</tr>
														<tr>
															<td  width="30%"  valign="top"  align="right">
																Ваш аватар: 
															</td>
															<td  width="70%"  valign="top"  align="left">
																													<div  id="image_address_label"  style="height: 24px;">
																</div>
																<input   type="file"  accept="image/*"  name="img_file"  style="opacity: 0; filter:alpha(opacity:0); width: 1px; height: 1px; padding:0; margin:0"                                         onchange="$('#image_address_label').html(this.value.split('\\')[this.value.split('\\').length-1]);">
																 																										<a  href="javascript://"  onclick="document.settings_form.img_file.click(); return false;">
																	<?php if ($AVATAR_URL) {echo '<img src="/avatar/user/'.$id_user_now.'/'.$AVATAR_URL.'" width="220" border="0" />';} else {echo '<img src="/avatar/no_img.jpg" width="220" border="0" />';} ?>														
																</a>
																													
															</td>
															
														</tr>
														<tr>
															<td  colspan="2"  align="center">
																<input  type="submit"  value="Сохранить"  name="user_edit"  style="font-weight:bold;" >
																
															</td>
															
														</tr>
														
													</table>
													
												</form>
												
											</td>
											
										</tr>
										<tr>
											<td  colspan="2">
											</td>
											
										</tr>
										
									</table>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
			<!-- END BODY -->
			<?php require_once('../global/footer.php');?>		
		</body>
	</html>