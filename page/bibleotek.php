<?php
include('../config/security.php');
include('../global/mySql.php');
$sortBy = $_POST['sortBy'];
//byName or byRating
$per_page=6;
$cur_page=1;
if (isset($_GET['page'])  &&  $_GET['page'] > 0) {
	$cur_page = $_GET['page'];
}
$start = ($cur_page - 1) * $per_page;
//первый оператор для LIMIT
//общее число книг
$res = mysqli_query($dbc,"SELECT * FROM `books`");
$rows = mysqli_num_rows($res);
//число страниц
$num_pages = ceil($rows / $per_page);
$end1=$start+$per_page;
$query = 'SELECT *   FROM `books` WHERE `published`=1  ORDER BY `title`  ASC LIMIT '.$start.' , '.$end1;
$data = mysqli_query($dbc,$query);
if(!$data){
	echo      "<p class='text'>Книга не найдена Код ошибки:</p>";
	echo      exit(mysqli_error($dbc));
	
}
// зададим переменную, которую будем использовать для вывода номеров страниц
$page = 0;

$output = "";
if (mysqli_num_rows($data) > 0){
			foreach ($data  as  $row)		{
		$title=$row["title"];
		$diskname=$row["diskname"];
		$img=$row["img_cover"];
		$output=$output.'				
		<div  class="material">			
		<div								
		class="title_book"   align="center">	'.substr($title,0,37).'				
		</div>									
		<div   class="img_book"   align="center">
		<a   href="readMore.php?book='.$diskname.'"   title="Описание">			
		<img   src="'.$img.'"   width="200"   border="0" />	
		</a>							
		</div>									
		<div   class="tools_book">				
		<a  									
		href="readMore.php?book='.$diskname.'"   title="Описание">	Описание			
		</a>								
		<a  								
		href="readBook.php?page=0&book='.$diskname.'">	
		Читать						
		</a>						
		</div>							
		</div>	
		';
		
	}
										
}
?>
<!DOCTYPE    HTML>
<head>
	<meta      http-equiv="Content-Type"      content="text/html; charset=utf-8" >
	<title>
		myBook | Библиотека						
	</title>
	<link      rel="stylesheet"      href="../CSS/style.css"      type="text/css" >
	<link      href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,700,400&subset=latin,cyrillic'      rel='stylesheet'      type='text/css' >
	<script      src="../js/jQuery1-3.js">
		
	</script>
	<script      src="../js/enter.js">
		
	</script>
	
</head>
<body>
	<div   class="background">
		
	</div>
	<?php
	require_once('../global/header.php');
	?>
	<!-- BODY Here -->
	<div      align="center"      style="width:auto; height:auto; min-height:1450px;">
		<div>
			<table      width="980">
				<tr>
					<td      valign="bottom">
						<?php
						require_once('../global/menu.php');
						?>
						<div      id="main-content">
							<div      class="paper"    style="height: auto; min-height: 1300px;">
								<table      border="0"      width="100%"       class="table">
									<!-- Первая строка заголовок -->
									<tr>
										<td      valign="top"      align="left"      style="padding: 10px;">
											<div    align="left"    style="width:100%;">
												<h1>
													Библиотека MyBook																																																		
												</h1>
												<div style="display:inline-block; float: right;"><a  href="/page/bibleotek.php?page=<?php
														echo  $cur_page-1 ?>
														">
														« Предыдущая страница  
													</a>
													| 
													<a  href="/page/bibleotek.php?page=<?php
														echo  $cur_page+1 ?>
														">
														Следующая страница »
													</a></div>
												<a    href="../upload/fileList.php">
													список всех книг												
												</a>
												<div>
													Сортировка:													<select   id="sortBy" >
														<option    value="byName"   onClick="getBooks();">
															По названию																												
														</option>
														<option    value="byRating"   onClick="getBooks();">
															По рейтингу																												
														</option>
																											
													</select>
																									
												</div>
																							
											</div>
											<br/>																																																																													
										</td>
																			
									</tr>
									<tr>
										<td>
											<div   id="books">
												<?php
												echo   $output; ?>
												
											</div>
																					
										</td>
																			
									</tr>
									<tr>
										<td>
											<div>
												<center>
													<a  href="/page/bibleotek.php?page=<?php
														echo  $cur_page-1 ?>
														">
														« Предыдущая страница  
													</a>
													| 
													<a  href="/page/bibleotek.php?page=<?php
														echo  $cur_page+1 ?>
														">
														Следующая страница »
													</a>
																									
												</center>
												
											</div>
																					
										</td>
																			
									</tr>
																	
								</table>
															
							</div>
							<!-- end paper -->
							<!-- здесь может быть ваша реклама -->
						
													
						</div>
						<!-- end main-content -->
											
					</td>
									
				</tr>
							
			</table>
					
		</div>
			
	</div>
	<!-- END BODY -->
		<?php
							require_once('../global/footer.php');
							?>
</body>
</html>