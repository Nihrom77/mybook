<!DOCTYPE  HTML>
<?php
include('../config/security.php');
$book = $_GET['book'];
include('../global/mySql.php');
include('../handler/parserFb2.php');
$query = "SELECT *   FROM `books` WHERE `diskname` LIKE '$book' LIMIT 1";
$result = mysqli_query($dbc,$query);
if(!$result){
	echo    "<p class='text'>Книга не найдена Код ошибки:</p>";
	echo    exit(mysqli_error($dbc));
}
$myrow = mysqli_fetch_array($result);
$rating=$myrow["rating"];
$title=$myrow["title"];
$category=$myrow["category"];
$diskname=$myrow["diskname"];
$p = new ParserFb2('/book/'.$diskname);
$description =$p->getAnnotation();
$author=$myrow["author"];
$book_id=$myrow["book_id"];
$book_cover=$myrow["img_cover"];
?>
<head>
	<meta  http-equiv="Content-Type"  content="text/html; charset=utf-8" />		<title>
		myBook | Описание к книге
	</title>
	<link  rel="stylesheet"  href="../CSS/style.css"  type="text/css" />
	<link  rel="stylesheet"  href="../CSS/layout.css"  type="text/css" />		
	<link  href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,700,400&subset=latin,cyrillic'  rel='stylesheet'  type='text/css' />
	
	<script  src="../js/jQuery1-3.js"></script>
	<script  src="../js/enter.js"></script>
	
</head>
<body>
	<div  class="background">
	</div>
	<?php
	require_once('../global/header.php');
	?>
	<!-- BODY Here -->
	<div  align="center"  style="width:auto; min-height:2000px;">
		<div>
			<table  width="980">
				<tr>
					<td  valign="bottom">
						<?php
						require_once('../global/menu.php');
						?>
						<div  id="main-content">
							<div  class="paper">
								<table  border="0"  width="100%"   class="table">
									<tr>
										<td  valign="top"  align="left">
											<h1>
												<a  href="bibleotek.php">
													Библиотека
												</a>
												&raquo; Описание книги 
											</h1>
											<br>												  
										</td>
										
									</tr>
								</table>
								<table  width="100%">
									<tr>
										<td  valign="top"  align="center"  width="20%"  style="border-right:1px dotted #c8c8c8; padding:10px;">
											<img  src="<?php	echo($book_cover);?>"  width="250" />
											<br>
											<div  class="tools_book">
												<a  href="readBook.php?page=0&book=<?php
													echo  $diskname; ?>
													">
													Читать книгу online
												</a>
											</div>
										</td>
										<td  valign="top"  align="left">
											<table  width="100%"  class="about_book">
												<tr>
													<td  align="right"  width="1%">
														<span>
															Название:
														</span>
													</td>
													<td  align="left">
														<?php
														echo  $title ; ?>
													</td>
													
												</tr>
												<tr>
													<td  align="right"  width="1%">
														<span>
															Жанр:
														</span>
													</td>
													<td  align="left">
														<?php
														echo  $category; ?>
													</td>
													
												</tr>
												<tr>
													<td  align="right"  width="1%"  valign="top">
														<span>
															Описание:
														</span>
													</td>
													<td  align="left">
														<div  class="annotation">
															<?php
															echo  $description; ?>
														</div>
													</td>
													
												</tr>
												<tr>
													<td  align="right"  width="1%">
														<span>
															Автор:
														</span>
													</td>
													<td  align="left">
														<?php
														echo  $author; ?>
													</td>
													
												</tr>
												<tr>
													<td  align="right"  width="1%">
														<span>
															Рейтинг:
														</span>
													</td>
													<td  align="left">
														<span  id="rating">
															<?php
															echo  $rating; ?>
														</span>
													</td>
													
												</tr>
												
											</table>
											
										</td>
										
									</tr>
									<tr><!-- вторая строка внешней таблицы -->
										<td  valign="top"  colspan="2">
											<table  width="100%">
												<tr>
													<td  align="left"  colspan="2">
														<h3>
															Сообщение:
														</h3>
													</td>
													
												</tr>
												<tr>
													<td  valign="top"  align="left"  colspan="2">
														<textarea  id="text_comment"  class="rating" value=""></textarea>
													</td>
												</tr>
												<tr>
													<td  align="left"  valign="middle">
														Ваша оценка: <select  id="book_rating">
															<option  value="0">
																0
															</option>
															<option  value="1">
																1
															</option>
															<option  value="2">
																2
															</option>
															<option  value="3">
																3
															</option>
															<option  value="4">
																4
															</option>
															<option  value="5">
																5
															</option>
															<option  value="6">
																6
															</option>
															<option  value="7">
																7
															</option>
															<option  value="8">
																8
															</option>
															<option  value="9">
																9
															</option>
															<option  value="10">
																10
															</option>
														</select>
													</td>
													<td  align="right"  valign="middle">
														<input  type="hidden"  id="id_book"  value="<?php echo  $book_id; ?>" />
														<input  type="hidden"  id="id_user"  value="<?php session_start(); $ID=$_SESSION['id_online_user']; echo  $ID ?>" />
														<a  href="javascript://"  onClick="postCommentToServer('comment');"  class="button_post">
															Отправить
														</a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td  colspan="2">
											<div  id="comments">
												<?php
												require "../handler/getBookComments.php";
												?>
												
											</div>
											<!-- div comments ended -->
										</td>
									</tr>
									
								</table>
								
							</div><!-- div paper ended -->
							<?php
			require_once('../global/footer.php');
			?>
							
						</div><!-- div main-content ended -->
					</td>
					
				</tr>
				
				
			</table>
			
		</div>
		
	</div>
	<!-- END BODY -->
		
</body>
</html>