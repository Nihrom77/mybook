<?php
  include('../config/security.php');
  include('../global/mySql.php');
  session_start();
 if ($_GET['id']) {
 	$id_user_now=$_GET['id'];
 } else {
	 $id_user_now=$_SESSION['id_online_user']; // ид свой
 }
 
  mysqli_query($dbc,'SET NAMES utf8');
										
	$query = "SELECT *   FROM `users` WHERE `id`=$id_user_now";
$result = mysqli_query($dbc,$query);
if(mysqli_num_rows($result) == 0){
	echo   "<p class='text'>Такого пользователя не существует</p>";
	echo   mysqli_close($dbc);
}
if (mysqli_num_rows($result) == 1){
		$myrow = mysqli_fetch_array($result);
		
		$NAME=$myrow["Name"];
		$EMAIL=$myrow["Email"];
		$AVATAR_URL=$myrow["url_avatar"];
		$DATE = $myrow["date_of_birth"];
		$LOGIN = $myrow["login"];
		$KARMA=$myrow["karma"];
		
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Настройки профиля - myBook</title>
		<link rel="stylesheet" href="../CSS/style.css" type="text/css" />
		<link href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,700,400&subset=latin,cyrillic' rel='stylesheet' type='text/css' />
		<script src="../js/jQuery1-3.js"></script>
		<script src="../js/enter.js"></script>

	</head>
	<body>
		<div class="background"></div>

		<?php require_once('../global/header.php');?>

		<!-- BODY Here -->
			<div align="center" style="width:auto; min-height:1000px;">
			
			<div>
				<table width="980">
					<tr>
						<td valign="bottom">
					<?php
						require_once('../global/menu.php');
						?>
						<div id="main-content">
							<div class="paper">
								<div class="table_profile">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tr>
										<td colspan="2" align="right"><div  class="top_profile_title"><?php  echo $NAME;?></div></td>
									</tr>
									<tr>
										<td valign="top" align="left" width="20%"  height="600" style="background:#e9e0d2;">
											<?php require_once ('../global/menu_profile.php'); ?>
											
										</td>
										<td valign="top" align="left" width="80%">
											<table cellpadding="0" cellspacing="0" border="0" width="100%">
												<tr>
													<td valign="top" align="center" width="20%" height="600">
														<?php if ($AVATAR_URL) {echo '<img src="/avatar/user/'.$id_user_now.'/'.$AVATAR_URL.'" width="220" border="0" />';} else {echo '<img src="/avatar/no_img.jpg" width="220" border="0" />';} ?>
													<br />
													<div id="rating_user_<?php echo $id_user_now;?>" class="raiting_user" title="Рейтинг пользователя"><?php echo $KARMA; ?></div><br />
													<ul class="menu_under_avatar">
														<li><a href="/page/message_send.php?receiverName=<?php echo $NAME;?>&receiverID=<?php echo $id_user_now;?>" style="border-radius: 0 5px 0 0;" class="entypo-mail"> Написать сообщение</a></li>
														<li><a href="#" onClick="addToMyFriends(<?php echo $id_user_now;?>);" style="border-radius: 0 0 5px 0;" class="entypo-user-add"> Добавить в друзья</a></li>
													</ul>
													</td>
													<td valign="top" align="left" width="80%">
													Имя пользователя: <?php echo $NAME;?><br />
													Дата рождения: <?php echo $DATE;?><br />
													E-mail: <?php echo $EMAIL;?><br />
													</td>
												</tr>
												<tr>
													<td colspan="2" style="font-size:11px; color:#ccc; padding:5px;" align="center">Личный профиль <?php echo $LOGIN;?></td>
												</tr>
											</table>
											
										</td>
									</tr>
									<tr>
										<td colspan="2"></td>
									</tr>
								</table></div>
								</td>
					</tr>
				</table>
			</div>
		</div>

		<!-- END BODY -->
	
		<?php require_once('../global/footer.php');?>
		

	</body>
</html>

