
<!DOCTYPE HTML>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>MyFriend | Мои друзья</title>
		<link rel="stylesheet" href="../CSS/style.css" type="text/css" />
		<link href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,700,400&subset=latin,cyrillic' rel='stylesheet' type='text/css' />
<script src="../js/jQuery1-3.js"></script>
		<script src="../js/enter.js"></script>
		<script src="../js/searchFriends.js"></script>
	</head>
	<body>
		<div class="background"></div>

		<?php require_once('../global/header.php');?>

		<!-- BODY Here -->
			<div align="center" style="width:auto; min-height:1000px;">
			
			<div>
				<table width="980">
					<tr>
						<td valign="bottom">
					<?php require_once('../global/menu.php');?>
						<div id="main-content">
							<div class="paper">
								<div>
									<!-- поиск по людям -->
									<form name="search" method="POST" action="../page/allUsers.php" >
   										<input type="search" id="searchFriend_box" name="keyword" placeholder="Поиск друзей" class="search_input" autocomplete="off" >
    									<button type="button" onclick="searchFriend();" class="search_btn">Найти</button> 
									</form>
									<div id="searchFriends_advice_wrapper"></div>
								</div>
								<div>
										<div id="friendList">
									<?php require "../handler/getFriendsList.php" ?>
										</div>
								</div>
							</div>
						</div>
						</td>
					</tr>
				</table>
			</div>
		</div>

		<!-- END BODY -->
	
		<?php require_once('../global/footer.php');?>
		

	</body>
</html>

