<?php
include('../config/security.php');
include('../global/mySql.php');
if (isset($_GET['category'])) {
	$cur_category = $_GET['category'];

/* Выбираем все книги с категорией пришедшей */

$data = mysqli_query($dbc,"SELECT * FROM `books` WHERE `category`='$cur_category' AND `published`=1 LIMIT 6") or die(mysqli_error($dbc));
$output = "";
if (mysqli_num_rows($data) > 0){
			foreach ($data  as  $row)		{
				$title=$row["title"];
				$diskname=$row["diskname"];
				$img=$row["img_cover"];
				$output=$output.'				
					<div  class="material">			
					<div								
					class="title_book"   align="center">	'.substr($title,0,37).'				
					</div>									
					<div   class="img_book"   align="center">
					<a   href="readMore.php?book='.$diskname.'"   title="Описание">			
					<img   src="'.$img.'"   width="200"   border="0" />	
					</a>							
					</div>									
					<div   class="tools_book">				
					<a  									
					href="readMore.php?book='.$diskname.'"   title="Описание">	Описание			
					</a>								
					<a  								
					href="readBook.php?page=0&book='.$diskname.'">	
					Читать						
					</a>						
					</div>							
					</div>	
				';
		
	}
										
}else{

$output = "Книг этой категории у нас нет";	
}
}
?>
<!DOCTYPE    HTML>
<head>
	<meta      http-equiv="Content-Type"      content="text/html; charset=utf-8" >
	<title>
		myBook | Библиотека						
	</title>
	<link      rel="stylesheet"      href="../CSS/style.css"      type="text/css" >
	<link      href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,700,400&subset=latin,cyrillic'      rel='stylesheet'      type='text/css' >
	<script      src="../js/jQuery1-3.js">
		
	</script>
	<script      src="../js/enter.js">
		
	</script>
	
</head>
<body>
	<div   class="background">
		
	</div>
	<?php
	require_once('../global/header.php');
	?>
	<!-- BODY Here -->
	<div      align="center"      style="width:auto; height:auto; min-height:1450px;">
		<div>
			<table      width="980">
				<tr>
					<td      valign="bottom">
						<?php
						require_once('../global/menu.php');
						?>
						<div      id="main-content">
							<div      class="paper"    style="height: auto; min-height: 1300px;">
								<table      border="0"      width="100%"       class="table">
									<!-- Первая строка заголовок -->
									<tr>
										<td      valign="top"      align="left"      style="padding: 10px;">
											<div    align="left"    style="width:100%;">
												<h1>
													Библиотека MyBook	» <?php	echo $cur_category;	?>																																														
												</h1>
											
																							
											</div>
											<br/>																																																																													
										</td>
																			
									</tr>
									<tr>
										<td>
											<div   id="books">
												<?php
												echo   $output; ?>
												
											</div>
																					
										</td>
																			
									</tr>
									
																	
								</table>
															
							</div>
							<!-- end paper -->
							<!-- здесь может быть ваша реклама -->
						
													
						</div>
						<!-- end main-content -->
											
					</td>
									
				</tr>
							
			</table>
					
		</div>
			
	</div>
	<!-- END BODY -->
		<?php
							require_once('../global/footer.php');
							?>
</body>
</html>