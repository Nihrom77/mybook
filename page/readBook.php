<?php
session_start();
include('../config/security.php');
include  ('../handler/parserFb2.php');
?>
<!DOCTYPE  HTML  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta  http-equiv="Content-Type"  content="text/html; charset=utf-8" />		<title>
			myBook | Описание к книге
		</title>
		<link  rel="stylesheet"  href="../CSS/style.css"  type="text/css" />	
			<link  href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,700,400&subset=latin,cyrillic'  rel='stylesheet'  type='text/css' />
			<link rel="stylesheet" href="../CSS/colorpicker.css" type="text/css" />
			<link rel="stylesheet" href="../CSS/layout_colorpicker.css" type="text/css" />
		
			<script  src="../js/jQuery1-3.js"></script>
		<script type="text/javascript" src="../js/colorpicker.js"></script>
    <script type="text/javascript" src="../js/eye.js"></script>
    <script type="text/javascript" src="../js/utils.js"></script>
    <script type="text/javascript" src="../js/layout.js?ver=1.0.2"></script>
		</script>
		<style>
			.panel_UL {
				padding:0px; margin:0px; list-style:none;
				display:block; position:absolute; z-index: 9999;
			}
			.panel_UL li {
				float:left; padding:10px;
				
			}
			.font_Arial {
				font-family:Arial;
			}
			.font_Tahoma {
				font-family:Tahoma;
			}
			.font_Times {
				font-family: Times;
			}
			
		</style>
		<!-- 
				нужно будет в отдельный файл сделать например read_panel.js
					-->
		
		
	</head>
	<body style="background:#fff;">

		<?php
		require_once('../global/header.php');
		?>
		<?php
							require_once('../global/menu.php');
			?>
			<div style="padding-left: 65px; width:auto; padding-right: 20px;">			
				
				<div  style=" padding:10px; width:800px;font-size:11pt; display: block; background:#fff; border-radius: 0 0 5px 5px; position: fixed; margin: 53px auto; z-index: 990; left:25%; "  align="center">
										<?php
										$cur_page = isset($_GET['page']) ? $_GET['page'] : 0; if($cur_page<0){
											$cur_page = 0;
										}
										$cur_book = $_GET['book'];
										?>

										<a  href="readBook.php?page=<?php	echo  ($cur_page-1).'&book='.$cur_book; ?>">Предыдущая глава</a> 
									
										| <h2  style="display:inline">	Вы читаете <?php	echo  $cur_page+1;	?>	главу</h2>
										| <a  href="readBook.php?page=<?php 	echo  ($cur_page+1).'&book='.$cur_book; ?>">Следующая глава</a>
									| <a href="javascript://" onClick="$('#sets_read').fadeToggle();">Настройки</a>
									
										<div  style="display:none;" id="sets_read">
										<div style="display:block; height:120px;">
										<ul  class="panel_UL">
											<li>
												Шрифт: <select  id="shrift">
													<option onclick="changeStyleText();" value="Arial">
														Arial
													</option>
													<option onclick="changeStyleText();" value="Tahoma">
														Tahoma
													</option>
													<option onclick="changeStyleText();" value="Times">
														Times
													</option>
													
												</select>
											</li>
																			<li>
												Размер шрифта: <select  id="size_text">
													<option onclick="changeStyleText();" value="12px" <?php if($_SESSION['set_font_size']=='12px') { ?> selected="selected"<?php }?>>
														12px
													</option>
													<option onclick="changeStyleText();" value="13px" <?php if($_SESSION['set_font_size']=='13px') { ?> selected="selected"<?php }?>>
														13px
													</option>
													<option onclick="changeStyleText();" value="14px" <?php if($_SESSION['set_font_size']=='14px') { ?> selected="selected"<?php }?>>
														14px
													</option>
													<option onclick="changeStyleText();" value="15px" <?php if($_SESSION['set_font_size']=='15px') { ?> selected="selected"<?php }?>>
														15px
													</option>
													<option onclick="changeStyleText();" value="16px" <?php if($_SESSION['set_font_size']=='16px') { ?> selected="selected"<?php }?>>
														16px
													</option>
													<option onclick="changeStyleText();" value="17px" <?php if($_SESSION['set_font_size']=='17px') { ?> selected="selected"<?php }?>>
														17px
													</option>
													<option onclick="changeStyleText();" value="18px" <?php if($_SESSION['set_font_size']=='18px') { ?> selected="selected"<?php }?>>
														18px
													</option>
													<option onclick="changeStyleText();" value="19px" <?php if($_SESSION['set_font_size']=='19px') { ?> selected="selected"<?php }?>>
														19px
													</option>
													<option onclick="changeStyleText();" value="20px" <?php if($_SESSION['set_font_size']=='20px') { ?> selected="selected"<?php }?>>
														20px
													</option>
												</select>
											</li>
											<li>
												Высота строки: <select  id="line_height">
													<option onclick="changeStyleText();" value="20px" <?php if($_SESSION['set_line_height']=='20px') { ?> selected="selected"<?php }?>>
														20px
													</option>
													<option onclick="changeStyleText();" value="22px" <?php if($_SESSION['set_line_height']=='22px') { ?> selected="selected"<?php }?>>
														22px
													</option>
													<option onclick="changeStyleText();" value="24px" <?php if($_SESSION['set_line_height']=='24px') { ?> selected="selected"<?php }?>>
														24px
													</option>
													<option onclick="changeStyleText();" value="28px" <?php if($_SESSION['set_line_height']=='28px') { ?> selected="selected"<?php }?>>
														28px
													</option>
													<option onclick="changeStyleText();" value="30px" <?php if($_SESSION['set_line_height']=='30px') { ?> selected="selected"<?php }?>>
														30px
													</option>
													<option onclick="changeStyleText();" value="32px" <?php if($_SESSION['set_line_height']=='32px') { ?> selected="selected"<?php }?>>
														32px
													</option>
													<option onclick="changeStyleText();"  value="34px" <?php if($_SESSION['set_line_height']=='34px') { ?> selected="selected"<?php }?>>
														34px
													</option>
												</select>
											</li>
											<li>
												<table>
													<tr>
														<td valign="top">Цвет фона:</td>
														<td valign="top"><div id="colorSelector" style="display:inline-block;"><div style="background-color: <?php if(isset($_SESSION['set_background'])) { echo $_SESSION['set_background'];} else {?>#ffffff<?php } ?>"></div></div></td>
													</tr>
												</table>
											</li>
											<li>
												Цвет текста: <input type="text" maxlength="6" size="6" id="colorpickerField1" value="<?php if(isset($_SESSION['set_color'])) { echo substr($_SESSION['set_color'],1);} else {?>000000<?php }?>" />
											</li>
											<li>
												Выбор темы: <select  id="theme">
													<option onclick="changeStyleText();" value="none" <?php if($_SESSION['set_theme']=='none') { ?> selected="selected"<?php }?>>
														Не выбрана
													</option>
													<option onclick="changeStyleText();" value="Dark" <?php if($_SESSION['set_theme']=='Dark') { ?> selected="selected"<?php }?>>
														Ночная тема
													</option>
													<option  value="Light" <?php if($_SESSION['set_theme']=='Light') { ?> selected="selected"<?php }?>>
														Дневная тема
													</option>
													<option onclick="changeStyleText();"  value="DarkBlue" <?php if($_SESSION['set_theme']=='DarkBlue') { ?> selected="selected"<?php }?>>
														Темно-синяя
													</option>
													<option  value="Violet" <?php if($_SESSION['set_theme']=='Violet') { ?> selected="selected"<?php }?>>
														Фиолетовая
													</option>
													<option onclick="changeStyleText();" value="Green" <?php if($_SESSION['set_theme']=='Green') { ?> selected="selected"<?php }?>>
														Зеленая
													</option>
													<option  value="Blue" <?php if($_SESSION['set_theme']=='Blue') { ?> selected="selected"<?php }?>>
														Синяя
													</option>
													<option onclick="changeStyleText();" value="Sweet" <?php if($_SESSION['set_theme']=='Sweet') { ?> selected="selected"<?php }?>>
														Сладкая
													</option>
													<option  value="LightBlue" <?php if($_SESSION['set_theme']=='LightBlue') { ?> selected="selected"<?php }?>>
														Мятная
													</option>													
													<option onclick="changeStyleText();" value="Beige" <?php if($_SESSION['set_theme']=='Beige') { ?> selected="selected"<?php }?>>
														Бежевая
													</option>
													<option  onclick="changeStyleText();" value="LightViolet" <?php if($_SESSION['set_theme']=='LightViolet') { ?> selected="selected"<?php }?>>
														Светло-фиолетовая
													</option>
													<option onclick="changeStyleText();" value="LightGreen" <?php if($_SESSION['set_theme']=='LightGreen') { ?> selected="selected"<?php }?>>
														Светло-зеленая
													</option>													
												</select>
											</li>

										</ul>
										</div>
										<div id="ConfirmBtn" style="display: block;width:auto;" align="center"><input  type="button"  value="Применить настройки"   onclick="changeStyleText();" />		</div>
									</div>
									</div>				
				
								
									<div  class="readText"  id="readText" align="left" style="padding-top: 100px;">
										<?php
										$parser = new ParserFb2('/book/'.$cur_book);
										$next_page = $_GET['page'];
										if($next_page<0){
											echo  $parser->getTagsSection(0, 1);
										}
										else {
											echo  $parser->getTagsSection($next_page, 1);
										}
										?>
										
									</div>
		</div>
		<!-- BODY Here -->

<script>
		<?php
	
		 	 $color=$_SESSION['set_color'];
			$font_family = $_SESSION['set_font_family'];
			$font_size =  $_SESSION['set_font_size'];
			 $background = $_SESSION['set_background'];
			 $line_height = $_SESSION['set_line_height'];
			 $theme = $_SESSION['set_theme'];
		 	?>
		 	<?php if(isset($_SESSION['set_background'])) { if ($_SESSION['set_theme']=='none') {?>
		 	$('.readText').css({"font-size":<?php echo "'$font_size'";?>,'color':<?php  echo "'$color'"; ?>,"line-height":"<?php echo $line_height ?>"});
		 	$('body').css({"background-color": <?php echo "'$background'"; ?>});
		 	$('.readText').addClass('font_'+<?php echo "'$font_family'";?>);
			<?php } else {echo "changeTheme('".$_SESSION['set_theme']."');";} }?>
			function  changeStyleText() {
				if ($('#theme').val()=='none') {
				$(".readText").removeClass("font_Arial font_Tahoma font_Times");
				var background_text = $('#colorSelector div').css('backgroundColor');
				var color_text = '#'+$('#colorpickerField1').val();
				var lineHeight = $('#line_height').val();
				$('.readText').css({"font-size":$('#size_text').val(),'color':color_text,"line-height":lineHeight});
				$('.readText').addClass('font_'+$('#shrift').val());
				$('body').css({"background-color": background_text});
				} else {
					changeTheme($('#theme').val());
				}
				postSettings();
			}
			function changeTheme(name) {
				var Light = ["Arial","15px","24px","#FFFFFF","#6b636b"];
				var Dark = ["Arial","15px","24px","#363636","#c7c7c7"];
				var DarkBlue = ["Arial","15px","24px","#070729","#7fc46c"];
				var Violet = ["Arial","15px","24px","#290229","#00c4c4"];
				var Green = ["Arial","15px","24px","#0b2105","#d1d171"];
				var LightBlue = ["Arial","15px","24px","#c5ede9","#0c1145"];
				var Beige = ["Arial","15px","24px","#f8ff94","#113e0b"];
				var LightViolet = ["Arial","15px","24px","#fab9fa","#000000"];
				var Blue = ["Arial","15px","24px","#161657","#e0e084"];
				var Sweet = ["Arial","15px","24px","#242424","#ff00a6"];
				var LightGreen = ["Arial","15px","24px","#aaffa8","#2b0000"];
				var change = eval(name);
				$('.readText').css({"font-size":change[1],'color':change[4],"line-height":change[2]});
				$('.readText').addClass('font_'+change[0]);
				$('body').css({"background-color": change[3]});
			}
			function postSettings() {
				  $.post(
				  "/handler/settings_read.php",
				  {
				    color: '#'+$('#colorpickerField1').val(),
				    fontfamily: $('#shrift').val(),
				    fontsize: $('#size_text').val(),
				    background: $('#colorSelector div').css('backgroundColor'),
				    lineHeight : $('#line_height').val(),
				    theme: $('#theme').val()
				    
				  },
				  onAjaxSuccess
				  );
				}
				
				function onAjaxSuccess(data){
				  if (data=="Настройки сохранены"){
				        
				  } else {	
				alert(data);
				  }
				}

			
		</script>
			<script>
			$('#colorpickerField1').ColorPicker({
				<?php if(isset($_SESSION['set_color'])) {?>
				color:'#00000',
				<?} else {
					echo "color:' ".$_SESSION['set_color']."',";
				}
					?>
					
				onSubmit: function(hsb, hex, rgb, el) {
				$(el).val(hex);
				$(el).ColorPickerHide();
			},
			onBeforeShow: function () {
				$(this).ColorPickerSetColor(this.value);
			},
			onChange: function (hsb, hex, rgb) {
			$('#colorpickerField1').val(hex);
		$('#colorpickerField1').css({'color':'#' + hex,'backgroundColor':$('#colorSelector div').css('backgroundColor')});
	}
		})
		.bind('keyup', function(){
			$(this).ColorPickerSetColor(this.value);
		});

	$('#colorSelector').ColorPicker({
	color: '#ffffff',
	onShow: function (colpkr) {
		$(colpkr).fadeIn(500);
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut(500);
		return false;
	},
	onChange: function (hsb, hex, rgb) {
		$('#colorSelector div').css('backgroundColor', '#' + hex);
	}
});
			</script>
	</body>
</html>