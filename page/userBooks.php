<!DOCTYPE  HTML>
<?php 
require "../handler/getUserBooks.php"
	

 ?>
<head>
	<meta    http-equiv="Content-Type"    content="text/html; charset=utf-8" >
	<title>
		Настройки профиля - myBook				
	</title>
	<link   rel="stylesheet"    href="../CSS/style.css"    type="text/css" >
	<link   rel="stylesheet"   href="../CSS/book_cover_style.css"   media="all"   type="text/css" />   
	<link   rel="stylesheet"    href="http://fonts.googleapis.com/css?family=Ubuntu:300italic,700,400&subset=latin,cyrillic"      type="text/css" >
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
				
	<script    src="../js/enter.js"></script>

	<script    type="text/javascript">
(function($) {  
$(function() {  
  $('ul.tabs').on('click', 'li:not(.current)', function() {  
    $(this).addClass('current').siblings().removeClass('current')  
      .parents('div.section').find('div.box').eq($(this).index()).fadeIn(150).siblings('div.box').hide();  
  })  
  
})  
})(jQuery)				
	</script>
	<style>
.box {  
  display: none; /* по умолчанию прячем все блоки */  
}  
.box.visible {  
  display: block; /* по умолчанию показываем нужный блок */  
} 
</style>
</head>
<body>
	<div    class="background">
				
	</div>
	<?php		require_once('../global/header.php');		?>		<!-- BODY Here -->
	<div    align="center"    style="width:auto; min-height:1000px;">
		<div>
			<table    width="980">
				<tr>
					<td    valign="bottom">
						<?php						require_once('../global/menu.php');						?>							<div    id="main-content">
							<div    class="paper">
								<div    class="table_profile">
									<table    width="100%"    cellpadding="0"    cellspacing="0">
										<tr>
											<td    colspan="2"    align="right">
												<div     class="top_profile_title">
													<?php														echo   $NAME;														?>																											
												</div>
																								
											</td>
																						
										</tr>
										<tr>
											<td    valign="top"    align="left"    width="20%"     height="600"    style="background:#e9e0d2;">
												<?php													require_once   ('../global/menu_profile.php'); ?>																																					
											</td>
											<td    valign="top"    align="left"    width="80%">
												<table    cellpadding="0"    cellspacing="0"    border="0"    width="100%">
													<tr>
														<td    valign=width="20%" >
															<form     enctype="multipart/form-data" action="../upload/uploadPage.php"    method="POST"    >
																<input    type="hidden"    name="<?=ini_get(" session. upload_progress. name")?>"   
																<label  for="file">Добавить книгу:	</label>
																 <input type="hidden" name="MAX_FILE_SIZE" value="52428800" />
															<input    type="file"    name="file"    id="file">
															<br>
															<input    type="submit"    name="submit"    value="upload">
																														
															</form>
																												
														</td>
																										
													</tr>
													<tr>
													<td>
<div class="section">  
<div class="userbooks">
  <ul class="tabs">  
    <li class="current link">Книги, которые я уже прочитал	</li>  
    <li class="link">Книги, которые я читаю</li>  
	<li class="link">Книги, которые я планирую прочитать</li> 
	<li class="link">Книги, которые я пишу</li>
  </ul>  
  </div>
  <div class="box visible">  
    	<div>
					<h3>
						Книги, которые я уже прочитал			
						</h3>
																														
														</div>
														<?php	echo   $outputFutureBooks; ?>	 
</div>  
  <div class="box">  
   <div>
															<h3>
																Книги, которые я читаю
															</h3>
																														
														</div>
														<?php	echo   $outputNowBooks; ?>	
</div>  
 <div class="box">  
    <div>
															<h3>
																Книги, которые я планирую прочитать	
															</h3>
																														
														</div>
														<?php	echo   $outputLaterBooks; ?>
</div>  
<div class="box">  
    <div>
															<h3>
																Книги, которые я пишу
															</h3>
																														
														</div>
														<?php	echo   $outputMyBooks; ?>
</div> 
</div><!-- .section --> 
																												
													</td>
																										
													</tr>
													<tr>
													<td>
																																																								
													</td>
																										
													</tr>
													<tr>
													<td>
																																																									
													</td>
																										
													</tr>
													<tr>
													<td>
																																																									
													</td>
																										
													</tr>
																								
												</table>
																						
											</td>
																				
										</tr>
										<tr>
											<td    colspan="2">
																						
											</td>
																				
										</tr>
																		
									</table>
								</div><!-- table_profile ended -->								
							</div><!-- div paper ended -->
							<?php			require_once('../global/footer.php');			?>											
						
													
					</td>
											
				</tr>
									
			</table>
							
		</div>
		<!-- div main-content ended -->
					
	</div>
	<!-- END BODY -->
								
</body>
	
</html>