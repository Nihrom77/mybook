<?php
include('../config/security.php');
include('../global/mySql.php');
session_start();

 if ($_GET['receiverName'] && $_GET['receiverID']) {
	$receiverName=$_GET['receiverName'];
	$receiverID = $_GET['receiverID'];
	$subject = $_GET['subject'];
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
		<title>myBook | Написать сообщение</title>
		<link rel="stylesheet" href="../CSS/style.css" type="text/css" >
		<link href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,700,400&subset=latin,cyrillic' rel='stylesheet' type='text/css' >
		<link href="../CSS/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../CSS/froala_editor.min.css" rel="stylesheet" type="text/css">

<script src="../js/jQuery1-3.js"></script>
	<script src="../js/searchFriends.js"></script>
<script>

var suggest_count = 0;
var input_initial_value = '';
var suggest_selected = 0;
 
$(window).load(function(){
    // читаем ввод с клавиатуры
    $("#name_to").keyup(function(I){
        // определяем какие действия нужно делать при нажатии на клавиатуру
        switch(I.keyCode) {
            // игнорируем нажатия на эти клавишы
            case 13:  // enter
            case 27:  // escape
            case 38:  // стрелка вверх
            case 40:  // стрелка вниз
            break;
 
            default:
                // производим поиск только при вводе более 2х символов
                if($(this).val().length>0){
 
                    input_initial_value = $(this).val();
                    // производим AJAX запрос передаем ему GET query, в который мы помещаем наш запрос
                    $.get("../search/searchFriend.php", { "query":$(this).val() },function(data){
                        //php скрипт возвращает нам строку, ее надо распарсить в массив.
                        // возвращаемые данные: ['test:1','test1:2','test 2','test 3']

                        //var list = eval("("+data+")");
								var list = data.split(",");
                        suggest_count = list.length;
                        if(suggest_count > 0){
                            // перед показом слоя подсказки, его обнуляем
                            $("#searchFriends_advice_wrapper").html("").show();
                            for(var i in list){
                                if(list[i] !== ''){

                                    // добавляем слою позиции
																		//var name = list[i].split(":")[0];
                                    $('#searchFriends_advice_wrapper').append('<div class="advice_variant_message">'+list[i]+'</div>');
                                }
                            }
                        }
                    }, 'html');
                }
            break;
        }
    });
 
    //считываем нажатие клавишь, уже после вывода подсказки
    $("#name_to").keydown(function(I){
        switch(I.keyCode) {
            // по нажатию клавишь прячем подсказку
            case 13: // enter
            case 27: // escape
                $('#searchFriends_advice_wrapper').hide();
                return false;
            
            // делаем переход по подсказке стрелочками клавиатуры
            case 38: // стрелка вверх
            case 40: // стрелка вниз
                I.preventDefault();
                if(suggest_count){
                    //делаем выделение пунктов в слое, переход по стрелочкам
                    key_activate( I.keyCode-39 );
                }
            break;
        }
    });
 
    // делаем обработку клика по подсказке
    $('.advice_variant_message').live('click',function(){
        // ставим текст в input поиска
				var name1 =  $(this).text().split(":")[0]; 
				var id1 =  $(this).text().split(":")[1];
        $('#name_to').val(name1);
				$('#id_to').val(id1);
        // прячем слой подсказки
        $('#searchFriends_advice_wrapper').fadeOut(350).html('');
    });
 
    // если кликаем в любом месте сайта, нужно спрятать подсказку
    $('html').click(function(){
        $('#searchFriends_advice_wrapper').hide();
    });
    // если кликаем на поле input и есть пункты подсказки, то показываем скрытый слой
    $('#name_to').click(function(event){
        //alert(suggest_count);
        if(suggest_count)
            $('#searchFriends_advice_wrapper').show();
        event.stopPropagation();
    });
});
 
function key_activate(n){
    $('#searchFriends_advice_wrapper div').eq(suggest_selected-1).removeClass('active');
 
    if(n === 1 && suggest_selected < suggest_count){
        suggest_selected++;
    }else if(n === -1 && suggest_selected > 0){
        suggest_selected--;
    }
 
    if( suggest_selected > 0){
        $('#searchFriends_advice_wrapper div').eq(suggest_selected-1).addClass('active');
        $("#name_to").val( $('#searchFriends_advice_wrapper div').eq(suggest_selected-1).text() );
    } else {
        $("#name_to").val( input_initial_value );
    }
}


</script>
	</head>
	<body>
		<div class="background"></div>

		<?php require_once('../global/header.php');?>

		<!-- BODY Here -->
			<div align="center" style="width:auto; min-height:1000px;">
			
			<div>
				<table width="980">
					<tr>
						<td valign="bottom">
					<?php
						require_once('../global/menu.php');
						?>
						<div id="main-content">
							<div class="paper">
 									<div class="panel_message" align="right" style="padding:10px;">
 										<a href="Messages.php">Входящие</a> | <a href="message_out.php">Отправленные</a> |  <a href="message_send.php">Написать сообщение</a>
 									</div>
 									
 									<h1>Написать сообщение</h1>
 									Тема письма: <input type="text" id="title" name="title" value='<?php echo $subject; ?>' /><br />
 									Кому: <input type="text" id="name_to" value='<?php echo $receiverName; ?>' name="name_to"/>
									<div id="searchFriends_advice_wrapper"></div>												
									<input type="hidden" id="id_to" value='<?php echo $receiverID; ?>' name="id_to"/>
 								<section id="editor">
							      <div id='edit' style="margin-top: 30px;">
							
							      </div>
							  </section>
 									<input type="button" value="Отправить сообщение" onClick="sendMessage();" />
							</div>
						</div></td>
					</tr>
				</table>
			</div>
		</div>
<script src="../js/froala_editor.min.js"></script>

  <script>
      $(function(){
          $('#edit').editable({inlineMode: false})
      });
      function sendMessage() {
      	var msg = $("#edit").editable("getHTML").toString();
      	var to = $('#id_to').val();
      	var title = $('#title').val();
      	var from = '<?php echo $_SESSION['id_online_user'];?>';
      	$.post(
				  "/handler/send_message.php",
				  {
				    send_from : from,
				    send_to : to,
				    send_title : title,
				    send_msg : msg
				    
				  },
				  onAjaxSuccess1
				  );
				}
		function onAjaxSuccess1(data){
				document.location.href = "http://mybook.bugs3.com/page/Messages.php";
				}
     

  </script>

		<!-- END BODY -->
	
		<?php require_once('../global/footer.php');?>
		

	</body>
</html>

