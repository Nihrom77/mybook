function postForm() {
  $.post(
  "/handler/login_handler.php",
  {
    login: $('#login_value').val(),
    password: $('#password_value').val()
    
  },
  onAjaxSuccess
  );
}

function onAjaxSuccess(data){
  if (data=="Авторизация прошла успешно"){
        document.location.href = "/page/mainPage.php";
  } else {	
 $('#error').html(data);
  }
}
function postFormRegistration(){
  $.post(
    "/handler/register_handler.php",
  {
    name: $('#name').val(),
    login: $('#login').val(),
    password: $('#password').val(),
    passwordToo: $('#passwordToo').val(),
    birthDay: $('#birthDay').val(),
    email: $('#email').val(),
	captcha: $('#captcha').val()
  },
  registrationSuccess
  );
}

function registrationSuccess(data){
  if (data=="Продолжить регистрацию"){
        document.location.href = "http://mybook.bugs3.com";
  } else {
       
	var rnd = Math.round(Math.random()*(0-9999))+9999;
	$('#captcha_img').attr({"src":"/captcha.php?"+rnd});
	if (data !== 'update'){
	    $('#error').html(data);
	}
  }
}
function postCommentToServer(val) {
		
	if (val=='comment') {
		$.post(
			    "/handler/postBookComment.php",
			  {
			    id_user: $('#id_user').val(),
			    text_comment: $('#text_comment').val(),
			   id_book: $('#id_book').val(),
			    rating: $('#book_rating').val(),
			    
			  },
			  commentsHandler
			  );
	}
	
}

function commentsHandler(data) {
	if(data !== "Error: Num of comments is zero"){
	$("#comments").after(data.substring(0, data.indexOf("</div>")));
	document.getElementById("rating").innerHTML = data.substring(data.indexOf("</div>")+6 );
	document.getElementById("text_comment").value = "";
}

}

function contactform() {
//									var product = {
//									 email: $('#email').val(),
//    						tema: $('#tema').val(),
//    						message: $('#message').val()
//              };
//									alert(product.toSource());
				$.post(
     "/handler/form_processing.php",
  {
   email: $('#email').val(),
    tema: $('#tema').val(),
    message: $('#message').val()
  },
   contactSuccess
   );
   
}

function contactSuccess(data){

  if (data=="Ваше сообщение отправлено"){
       showModal('success',data,0);
  } else {
        $('#error').html(data);
  }
}
function setCommentVote(comment_id, vote, user_id, book_id)
{
    // ajax-крутилка
    $("#comment_num_vote_"+comment_id+"_note").css({display: "inline"});
    $("#comment_num_vote_"+comment_id+"_note").css({visibility: "visible"});

    $.post("/handler/handler_comment.php", {comment_id: comment_id, vote: vote, user_id: user_id, book_id: book_id}, 
		function(resp){
		if(resp == "Нельзя ставить оценки самому себе"){
		alert(resp);			
}else{
        // ajax-крутилка; В ответ приходят новые числа типа "8+1 / 24"
        $("#comment_num_vote_"+comment_id+"_note").css({display: "none"});
        $("#comment_num_vote_"+comment_id+"_note").css({visibility: "hidden"});
			
			
			document.getElementById("comment_num_vote_"+comment_id).innerHTML = resp.replace(/\n/g, "");
}
    });
}
function getNextPage(nextPageNum,cur_book)
{
    $.post("/handler/bookScrollingHandler.php", {nextPageNum: nextPageNum, cur_book:cur_book}, 
		function(resp){
		if(resp == "ошибка"){
		alert(resp);			
}else{
        // ajax-крутилка; В ответ приходят  новые 1000 символов текста"

			document.getElementById("bookText").innerHTML = resp;
}
    });

}
function restorePassword()
{
 $.post(
  "/handler/handler_lostpass.php",
  {
    email: $('#email').val(),
  },
 		function(resp){
		if(resp == "Пользователя с таким e-mail не существует"){
		alert(resp);			
}else{
        // ajax-крутилка;

			document.getElementById("error").innerHTML = resp;
}
    }
  );
}
function getBooks()
{
 $.post(
  "/handler/handler_getMoreBooks.php",
  {
    sortBy: $('#sortBy').val(),
  },
 		function(resp){
		if(resp == "ошибка"){
		alert(resp);			
}else{
        // ajax-крутилка;
			document.getElementById("books").innerHTML = resp;
}
    }
  );
}
function addToMyBooks(book_id)
{
$.post("/handler/addToUserBooks.php", {book_id: book_id}, 
		function(resp){
		if(resp !== ""){
		alert(resp);			
}
    });
}
function addToMyFriends(friend_id)
{
$.post("/handler/addToFriends.php", {friend_id: friend_id}, 
		function(resp){
	
		alert(resp);			

    });
}
function confirmFriendship(friend_id)
{
$.post("/handler/confirmFriendship.php", {friend_id: friend_id}, 
		function(resp){
		
		alert(resp);			
		 document.location.href = "/page/MyFriends.php";
    });
}
function deleteFriend(friend_id)
{
$.post("/handler/deleteFriendship.php", {friend_id: friend_id}, 
		function(resp){
		
		alert(resp);			
		 document.location.href = "/page/MyFriends.php";
    });
}
function editBook(diskname)
{
	//alert(diskname);	
	document.location.href = '/page/WriteBook.php?diskname='+diskname;
}