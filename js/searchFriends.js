
var suggest_count = 0;
var input_initial_value = '';
var suggest_selected = 0;
 
$(window).load(function(){
    // читаем ввод с клавиатуры
    $("#searchFriend_box").keyup(function(I){
        // определяем какие действия нужно делать при нажатии на клавиатуру
        switch(I.keyCode) {
            // игнорируем нажатия на эти клавишы
            case 13:  // enter
            case 27:  // escape
            case 38:  // стрелка вверх
            case 40:  // стрелка вниз
            break;
 
            default:
                // производим поиск только при вводе более 1х символов
                if($(this).val().length>0){
 
                    input_initial_value = $(this).val();
                    // производим AJAX запрос передаем ему GET query, в который мы помещаем наш запрос
                    $.get("../search/searchFriend.php", { "query":$(this).val() },function(data){
                        //php скрипт возвращает нам строку, ее надо распарсить в массив.
                        // возвращаемые данные: ['test','test 1','test 2','test 3']

                        //var list = eval("("+data+")");
								var list = data.split(",");
                        suggest_count = list.length;
                        if(suggest_count > 0){
                            // перед показом слоя подсказки, его обнуляем
                            $("#searchFriends_advice_wrapper").html("").show();
                            for(var i in list){
                                if(list[i] !== ''){

                                    // добавляем слою позиции
                                    $('#searchFriends_advice_wrapper').append('<div class="advice_variant">'+list[i]+'</div>');
                                }
                            }
                        }
                    }, 'html');
                }
            break;
        }
    });
 
    //считываем нажатие клавишь, уже после вывода подсказки
    $("#searchFriend_box").keydown(function(I){
        switch(I.keyCode) {
            // по нажатию клавишь прячем подсказку
            case 13: // enter
            case 27: // escape
                $('#searchFriends_advice_wrapper').hide();
                return false;
            
            // делаем переход по подсказке стрелочками клавиатуры
            case 38: // стрелка вверх
            case 40: // стрелка вниз
                I.preventDefault();
                if(suggest_count){
                    //делаем выделение пунктов в слое, переход по стрелочкам
                    key_activate( I.keyCode-39 );
                }
            break;
        }
    });
 
    // делаем обработку клика по подсказке
    $('.advice_variant').live('click',function(){
        // ставим текст в input поиска
				var friendID = $(this).text().split(":")[1];
				document.location.href = "/page/profile.php?id="+friendID;      
//$('#searchFriend_box').val($(this).text());
        // прячем слой подсказки
        //$('#searchFriends_advice_wrapper').fadeOut(350).html('');
    });
 
    // если кликаем в любом месте сайта, нужно спрятать подсказку
    $('html').click(function(){
        $('#searchFriends_advice_wrapper').hide();
    });
    // если кликаем на поле input и есть пункты подсказки, то показываем скрытый слой
  //  $('#searchFriend_box').click(function(event){
  //      //alert(suggest_count);
  //      if(suggest_count)
  //          $('#searchFriends_advice_wrapper').show();
  //      event.stopPropagation();
 //   });
});
 
function key_activate(n){
    $('#searchFriends_advice_wrapper div').eq(suggest_selected-1).removeClass('active');
 
    if(n === 1 && suggest_selected < suggest_count){
        suggest_selected++;
    }else if(n === -1 && suggest_selected > 0){
        suggest_selected--;
    }
 
    if( suggest_selected > 0){
        $('#searchFriends_advice_wrapper div').eq(suggest_selected-1).addClass('active');
        $("#searchFriend_box").val( $('#searchFriends_advice_wrapper div').eq(suggest_selected-1).text() );
    } else {
        $("#searchFriend_box").val( input_initial_value );
    }
}
function searchFriend(){
 $.get("../search/searchFriend.php", {query: $('#searchFriend_box').val(),flag:'1'}, 
		function(resp){
		$('#friendList').html(resp);
}
);
}