<?php
 if ($_GET['receiver']) {
	$receiver=$_GET['receiver'];
	
}
?>
<!DOCTYPE  HTML >
	<head>
		<meta  http-equiv="Content-Type"  content="text/html; charset=utf-8" >
		<title>
			Messages | Мои сообщения
		</title>
		<link  rel="stylesheet"  href="../CSS/style.css"  type="text/css" >
		<link  rel="stylesheet"  href="../CSS/messages_style.css"  type="text/css" >
		<link  href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,700,400&subset=latin,cyrillic'  rel='stylesheet'  type='text/css' >
		<script  src="../js/jQuery1-3.js">
		</script>
		<script  src="../js/enter.js">
		</script>
		
	</head>
	<body>
		<div  class="background">
		</div>
		<?php
		require_once('../global/header.php');
		?>
		<!-- BODY Here -->
		<div  align="center"  style="width:auto; min-height:1000px;">
						<div>
				<table  width="980">
					<tr>
						<td  valign="bottom">
							<?php
							require_once('../global/menu.php');
							?>
							<div  id="main-content">
								<div  class="paper">
									<div>
										<h1>Сообщения</h1>
									</div>
									<hr>
									<table>
										<tr>
											<td>
												<div><a href="/messages/newMessage.php" >Написать новое сообщение</a></div>
												<div><a href="/messages/inbox.php" >Входящие</a></div>
												<div><a href="/messages/sent.php" >Исходящие</a></div>
											</td>
											<td>
												<table>
						<form method="post" action="/global/contactform_action.php" id="contactform">
							<tr>
								<td valign="middle" align="right">Кому:</td>
								<td valign="middle" align="left"><input type="text" name ="receiver" class="" value='<?php echo $receiver; ?>' /></td>
							</tr>
							<tr>
								<td valign="middle" align="right">Тема</td>
								<td valign="middle" align="left"><input type="text" name = "subject" class="" /></td>
							</tr>
							<tr>
								<td valign="middle" align="right">Сообщение:</td>
								<td valign="middle" align="left"><textarea id="message_textArea" class="" name="messages"></textarea></td>
							</tr>
							<tr>
								<td valign="middle" align="right" colspan="2">
									  <input type="submit" value="Отправить" name="submit" class="" />

								</td>
							</tr>
						</form>
						</table>
											</td>
										</tr>
									</table
								</div>
								
							</div>
						</td>
						
					</tr>
					
				</table>
				
			</div>
			
		</div>
		<!-- END BODY -->
				<?php
		require_once('../global/footer.php');
		?>
			
	</body>
</html>